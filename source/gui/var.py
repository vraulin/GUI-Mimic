from typing import Any, Callable, TypeVar


T = TypeVar("T")

class freeVar:

    def __init__(self, value : T = "") -> None:
        self.value = value
        self._readCallbacks = []
        self._writeCallbacks = []

    
    def get(self) -> T:
        for func in self._readCallbacks:
            func()
        return self.value
    

    def set(self, value : T) -> None:
        self.value = value
        for func in self._writeCallbacks:
            func()
    

    def trace_add(self, mode : str, callback : Callable[[], Any]) -> None:
        if mode not in ("read", "write"):
            raise ValueError("Mode should be either 'read' or 'write'.")
        if mode == "read":
            self._readCallbacks.append(callback)
        else:
            self._writeCallbacks.append(callback)