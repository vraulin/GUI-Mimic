import tkinter as tk
from ..objects.user_guide import UserGuide

class ScenarioManager(tk.Toplevel):

    def link(self, guide : UserGuide, sce : str):
        self.title("Scenario : " + sce)
        self.name = sce
        self.scenario = getattr(guide, sce)
        self.guide = guide
        from ..objects.user_scenario import UserScenario
        if not isinstance(self.scenario, UserScenario):
            from tkinter import messagebox
            messagebox.showerror(message="An object already has this name and is not a scenario")
            self.destroy()
        
        import tkinter as tk
        
        self.stepList = tk.Listbox(self, selectmode="single")
        self.scrollBar = tk.Scrollbar(self, command=self.stepList.yview)
        self.stepList['yscrollcommand'] = self.scrollBar.set
        self.stepList.bind("<<ListboxSelect>>", self.updateInfo)

        self.scenarioLabel = tk.Label(self, text="Scenario name:")
        self.scenarioName = tk.Variable(self, value=sce)
        self.scenarioNameZone = tk.Entry(self, textvariable=self.scenarioName)
        self.scenarioNameZone.bind("<FocusOut>", self.updateName)
        self.scenarioNameZone.bind("<Return>", self.updateName)

        self.entryZone = tk.Entry(self)

        self.insertButton = tk.Button(self, text="Insert", command=self.insert)
        self.replaceButton = tk.Button(self, text="Replace", command=self.replace)
        self.deleteButton = tk.Button(self, text="Delete", command=self.delete)
        self.moveUpButton = tk.Button(self, text="Move up", command=self.moveUp)
        self.moveDownButton = tk.Button(self, text="Move down", command=self.moveDown)

        self.infoZone = tk.Label(self, text="Some information")

        self.matchList = tk.Listbox(self, selectmode="none")
        self.matchScroll = tk.Scrollbar(self, command=self.matchList.yview)
        self.matchList["yscrollcommand"] = self.matchScroll.set
        
        self.iconbitmap("source/images/logo.ico")

        self.refresh(10)

        self.bind("<Configure>", self.refresh)
        self.minsize(500, 300)
    

    def current(self) -> int:
        current = self.stepList.curselection()
        if current:
            return current[0]
        return -1
    

    def getEntry(self) -> str:
        import re
        try:
            expr = re.compile(self.entryZone.get())
        except:
            return ""
        return self.entryZone.get()


    def insert(self, event = None):
        current = self.current()
        if current < 0:
            current = 0
        entry = self.getEntry()
        if not entry:
            from tkinter import messagebox
            messagebox.showerror(message="Given expression has syntax errors.")
            return
        sequence = list(self.scenario)
        sequence.insert(current, entry)
        self.scenario._sequence = tuple(sequence)
        self.refresh()
        self.stepList.activate(current)
        self.master.change()


    def replace(self, event = None):
        current = self.current()
        if current >= 0:
            entry = self.getEntry()
            if not entry:
                from tkinter import messagebox
                messagebox.showerror(message="Given expression has syntax errors.")
                return
            sequence = list(self.scenario)
            sequence[current] = entry
            self.scenario._sequence = tuple(sequence)
            self.refresh()
            self.stepList.activate(current)
            self.master.change()


    def delete(self, event = None):
        current = self.current()
        if current >= 0:
            sequence = list(self.scenario)
            sequence.pop(current)
            self.scenario._sequence = tuple(sequence)
            self.refresh()
            self.master.change()


    def moveUp(self, event = None):
        current = self.current()
        if current > 0:
            sequence = list(self.scenario)
            sequence[current], sequence[current - 1] = sequence[current - 1], sequence[current]
            self.scenario._sequence = tuple(sequence)
            self.refresh()
            self.stepList.selection_set(current - 1)
            self.stepList.selection_clear(current)
            self.refresh()
            self.master.change()


    def moveDown(self, event = None):
        current = self.current()
        if current >= 0 and current < len(self.scenario._sequence) - 1:
            sequence = list(self.scenario)
            sequence[current], sequence[current + 1] = sequence[current + 1], sequence[current]
            self.scenario._sequence = tuple(sequence)
            self.refresh()
            self.stepList.selection_set(current + 1)
            self.stepList.selection_clear(current)
            self.refresh()
            self.master.change()
    

    def updateName(self, event = None):
        new_name = self.scenarioName.get()
        if hasattr(self.guide, new_name) and new_name != self.name:
            from tkinter import messagebox
            messagebox.showerror(message="The new scenario name already exists for another sequence or scenario")
            self.scenarioNameZone.focus_set()
            return
        if new_name != self.name:
            setattr(self.guide, new_name, self.scenario)
            delattr(self.guide, self.name)
            self.master.scenarioWindows.pop(self.name)
            self.master.scenarioWindows[new_name] = self
            self.name = new_name
            self.title("Scenario : " + self.name)
            self.master.change()
            self.master.refresh()
    

    def updateInfo(self, event = None):
        import re
        from ..format import duration
        current = self.current()
        self.matchList.delete(0, self.matchList.size())
        if current >= 0:
            matches = []
            expr = re.compile(self.scenario._sequence[current])
            for seq_name, seq in self.guide.sequences():
                if expr.fullmatch(seq_name):
                    matches.append(seq)
                    self.matchList.insert("end", seq_name)
            if matches:
                d1 = duration(min(seq.duration for seq in matches))
                d2 = duration(max(seq.duration for seq in matches))
                d3 = duration(sum(seq.duration for seq in matches) / len(matches))
                self.infoZone.config(text="Expression outcomes: {}\nMinimum duration: {}\nMaximum duration: {}\n Average duration: {}".format(len(matches), d1, d2, d3))
            else:
                self.infoZone.config(text="Expression outcomes: 0")
        else:
            outcomes = []
            for expr in self.scenario:
                matches = []
                expr = re.compile(expr)
                for seq_name, seq in self.guide.sequences():
                    if expr.fullmatch(seq_name):
                        matches.append(seq)
                        self.matchList.insert("end", seq_name)
                outcomes.append(matches)
                self.matchList.insert("end", "")
            size = 1
            for matches in outcomes:
                size *= len(matches)
            if size >= 2 ** 10:
                from random import choice
                chosen_outcomes = set()
                while len(chosen_outcomes) < 2 ** 10:
                    res = []
                    for matches in outcomes:
                        res.append(choice(matches))
                    chosen_outcomes.add(tuple(res))
            else:
                from itertools import product
                chosen_outcomes = set(product(*outcomes))
            if chosen_outcomes:
                d1 = duration(min(sum(seq.duration for seq in outcome) for outcome in chosen_outcomes))
                d2 = duration(max(sum(seq.duration for seq in outcome) for outcome in chosen_outcomes))
                d3 = duration(sum(sum(seq.duration for seq in outcome) for outcome in chosen_outcomes) / len(chosen_outcomes))
                self.infoZone.config(text="Scenario outcomes: {}\nMinimum duration: {}\nMaximum duration: {}\n Average duration: {}".format(len(chosen_outcomes), d1, d2, d3))
            else:
                self.infoZone.config(text="Scenario outcomes: 0")
        
    
    def refresh(self, times = 1):
        if not isinstance(times, int):
            times = 1
        current = self.current()
        for i in range(times):
            try:

                self.scenarioLabel.place(x = 0, y = 0, height = 20)
                self.scenarioNameZone.place(x = self.scenarioLabel.winfo_width() + 5, y = 0, height = 20)

                self.stepList.delete(0, self.stepList.size())
                for entry in self.scenario._sequence:
                    self.stepList.insert("end", entry)
                if current >= 0:
                    self.stepList.selection_set(current)
                else:
                    self.stepList.selection_set("end")

                self.stepList.place(x = 0, y = 25, height = self.winfo_height() - 25, width = self.winfo_width() / 2 - self.scrollBar.winfo_width())
                self.scrollBar.place(x = self.stepList.winfo_width(), y = 25, height = self.winfo_height() - 25)

                self.entryZone.place(x = self.winfo_width() / 2 + 10, y = 35, width = self.winfo_width() / 2 - 20)

                self.insertButton.place(x = self.entryZone.winfo_x(), y = self.entryZone.winfo_height() + 10 + self.entryZone.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)
                self.replaceButton.place(x = self.insertButton.winfo_x() + self.insertButton.winfo_width(), y = self.insertButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)
                self.deleteButton.place(x = self.replaceButton.winfo_x() + self.replaceButton.winfo_width(), y = self.insertButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)
                self.moveUpButton.place(x = self.insertButton.winfo_x() + self.insertButton.winfo_width() / 2, y = self.insertButton.winfo_y() + self.insertButton.winfo_height(), width = (self.winfo_width() / 2 - 20) / 3)
                self.moveDownButton.place(x = self.moveUpButton.winfo_x() + self.moveUpButton.winfo_width(), y = self.moveUpButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)
                
                self.infoZone.place(x = self.insertButton.winfo_x(), y = self.moveUpButton.winfo_y() + self.moveUpButton.winfo_height() + 10, width = self.winfo_width() / 2 - 20, height = 60)

                self.matchList.place(x = self.insertButton.winfo_x(), y = self.infoZone.winfo_y() + self.infoZone.winfo_height() + 10, width = self.winfo_width() / 2 - 20 - self.matchScroll.winfo_width(), height = self.winfo_height() - self.matchList.winfo_y())
                self.matchScroll.place(x = self.matchList.winfo_x() + self.matchList.winfo_width(), y = self.matchList.winfo_y(), height = self.matchList.winfo_height())

            except:
                pass
        self.updateInfo()
    

    def destroy(self) -> None:
        self.updateName()
        self.master.scenarioWindows.pop(self.name)
        return super().destroy()