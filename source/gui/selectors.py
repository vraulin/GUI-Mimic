from random import random
import tkinter as tk
from tkinter import ttk
from typing import Callable, Tuple

from source.objects.fuzz_utils import random_input





_name_translation_table = {
    "time" : "Duration", 
    "x" : "X coordinate",
    "y" : "Y coordinate",
    "button" : "Mouse button",
    "dx" : "X scroll",
    "dy" : "Y scroll",
    "speed" : "Input speed",
    "dt" : "Interval between clicks",
    "classes" : "Event classes to filter",
    "inverse" : "Filter mode",
    "inactivity" : "Inactivity threshold",
    "max_duration" : "Maximum path duration"
}

_listing_table = {
    "classes",
    "args"
}

def _time_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = float(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def convert(*args):
        entry = var.get()
        if not entry:
            entry = "0"
        trueVar.set(round(float(entry) * 1000000000))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("0")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter duration (in seconds):")
    if value != None:
        var.set(str(value / 1000000000))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
    return trueVar, place


def _speed_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = float(entry)
        except:
            return False
        if n <= 0:
            return False
        return True
    def convert(*args):
        entry = var.get()
        if not entry:
            entry = "3"
        trueVar.set(float(entry))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("3.0")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter speed (in keystrokes per seconds):")
    if value != None:
        var.set(str(value))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
    return trueVar, place


def _times_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = int(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def convert(*args):
        entry = var.get()
        if not entry:
            entry = "1"
        trueVar.set(int(entry))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("1")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter the number of clicks (ex: 2 for double-click):")
    if value != None:
        var.set(str(value))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
    return trueVar, place


def _key_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    var = freeVar()
    textVar = tk.Variable(master)
    def update(*args):
        textVar.set(str(var.get()))
    var.trace_add("write", update)
    from pynput.keyboard import Key
    var.set(Key.enter)
    def recordKey(*args):
        from pynput import keyboard
        key = [None]
        def listen(k):
            key[0] = k
            return False
        with keyboard.Listener(on_press = listen) as listener:
            listener.join()
        var.set(key[0])
    entry = tk.Entry(master, state="disabled", textvariable=textVar)
    recButton = tk.Button(master, text="Change", command=recordKey)
    if value != None:
        var.set(value)
    def place():
        recButton.place(x = master.winfo_width(), y = 0, anchor = "ne")
        entry.place(x = 0, y = 0, width = master.winfo_width() - recButton.winfo_width(), height = recButton.winfo_height())
    return var, place


def _button_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    var = freeVar()
    textVar = tk.Variable(master)
    def update(*args):
        textVar.set(str(var.get()))
    var.trace_add("write", update)
    from pynput.mouse import Button
    var.set(Button.left)
    def recordButton(*args):
        from pynput import mouse
        button = [None]
        def listen(x, y, b, pressed):
            button[0] = b
            return False
        with mouse.Listener(on_click = listen) as listener:
            listener.join()
        var.set(button[0])
    entry = tk.Entry(master, state="disabled", textvariable=textVar)
    recButton = tk.Button(master, text="Change", command=recordButton)
    if value != None:
        var.set(value)
    def place():
        recButton.place(x = master.winfo_width(), y = 0, anchor = "ne")
        entry.place(x = 0, y = 0, width = master.winfo_width() - recButton.winfo_width(), height = recButton.winfo_height())
    return var, place


def _input_select(master, value = None) -> Tuple[tk.Variable, Callable]:

    from .var import freeVar

    trueVar = freeVar()

    def setFromFixed(*args):
        trueVar.set(var.get())
    def setFromRandom(*args):
        from ..objects.fuzz_utils import random_sentence, random_word, random_input
        if mode.get() == "random word":
            trueVar.set(random_word)
        elif mode.get() == "random sentence":
            trueVar.set(random_sentence)
        elif mode.get() == "random input":
            trueVar.set(random_input)
        else:
            print("Decision error...")
        

    textVar = tk.Variable(master, value="Enter text to type in:")
    text = tk.Label(master, textvariable=textVar)

    var = tk.Variable(master)
    var.trace_add("write", setFromFixed)
    var.set("Hello world!")
    entry = tk.Entry(master, textvariable=var)

    randomWordText = tk.Label(master, text="Random English word")
    randomSentenceText = tk.Label(master, text="Random English sentence")
    randomInputText = tk.Label(master, text="Random utf-8 text")

    mode = tk.Variable(master, value="fixed")
    fixedModeButton = tk.Radiobutton(master, value="fixed", variable=mode, command=lambda *args: (entry.config(state="normal"), textVar.set("Enter text to type in:"), setFromFixed()))
    randomWordModeButton = tk.Radiobutton(master, value="random word", variable=mode, command=lambda *args: (entry.config(state="disabled"), textVar.set("Text will be an English word choosen randomly at execution"), setFromRandom()))
    randomSentenceModeButton = tk.Radiobutton(master, value="random sentence", variable=mode, command=lambda *args: (entry.config(state="disabled"), textVar.set("Text will be an English sentence choosen randomly at execution"), setFromRandom()))
    randominputModeButton = tk.Radiobutton(master, value="random input", variable=mode, command=lambda *args: (entry.config(state="disabled"), textVar.set("Text will be completely random utf-8 characters"), setFromRandom()))

    if value != None:
        if isinstance(value, str):
            var.set(value)
        else:
            from ..objects.fuzz_utils import random_sentence, random_word
            if value == random_word:
                mode.set("random word")
            elif value == random_sentence:
                mode.set("random sentence")
            elif value == random_input:
                mode.set("random input")

    def place():
        text.place(x = 0, y = 0)
        fixedModeButton.place(x = 0, y = text.winfo_height() + 5)
        entry.place(x = fixedModeButton.winfo_width(), y = text.winfo_height() + 5, width = master.winfo_width() - fixedModeButton.winfo_width(), height = fixedModeButton.winfo_height())
        randomWordModeButton.place(x = 0, y = fixedModeButton.winfo_y() + fixedModeButton.winfo_height() + 5)
        randomWordText.place(x = randomWordModeButton.winfo_width() + 5, y = randomWordModeButton.winfo_y(), height = randomWordModeButton.winfo_height())
        randomSentenceModeButton.place(x = 0, y = randomWordModeButton.winfo_y() + randomWordModeButton.winfo_height() + 5)
        randomSentenceText.place(x = randomSentenceModeButton.winfo_width() + 5, y = randomSentenceModeButton.winfo_y(), height = randomSentenceModeButton.winfo_height())
        randominputModeButton.place(x = 0, y = randomSentenceModeButton.winfo_y() + randomSentenceModeButton.winfo_height() + 5)
        randomInputText.place(x = randominputModeButton.winfo_width() + 5, y = randominputModeButton.winfo_y(), height = randominputModeButton.winfo_height())

    return trueVar, place


def _x_select(master, value = None) -> Tuple[tk.Variable, Callable]:

    from .var import freeVar

    trueVar = freeVar()

    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = int(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def recordClick(*args):
        from pynput import mouse
        coor = [None]
        def listen(x, y, button, pressed):
            coor[0] = x
            return False
        with mouse.Listener(on_click = listen) as listener:
            listener.join()
        var.set(str(coor[0]))
    def setFromFixed(*args):
        trueVar.set(int(var.get() or "0"))
    def setFromRange(*args):
        from ..objects.fuzz_utils import randrange
        a, b = fromVar.get(), toVar.get()
        if not a:
            return
        if not b:
            return
        a, b = int(a), int(b)
        if a > b:
            b, a = a, b
        trueVar.set(randrange(a, b))

    textVar = tk.Variable(master, value="Enter X coordinate (in pixels):")
    text = tk.Label(master, textvariable=textVar)

    var = tk.Variable(master)
    var.trace_add("write", setFromFixed)
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    recButton = tk.Button(master, text="Record", command=recordClick)

    fromText = tk.Label(master, text="From")
    fromVar = tk.Variable(master)
    fromVar.trace_add("write", setFromRange)
    toText = tk.Label(master, text="to")
    toVar = tk.Variable(master)
    toVar.trace_add("write", setFromRange)
    fromEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=fromVar, state="disabled")
    toEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=toVar, state="disabled")

    fromVar.set(0)
    toVar.set(20)
    var.set("0")

    mode = tk.Variable(master, value="fixed")
    fixedModeButton = tk.Radiobutton(master, value="fixed", variable=mode, command=lambda *args: (entry.config(state="normal"), recButton.config(state="normal"), textVar.set("Enter X coordinate (in pixels):"), fromEntry.config(state="disabled"), toEntry.config(state="disabled"), setFromFixed()))
    rangeModeButton = tk.Radiobutton(master, value="range", variable=mode, command=lambda *args: (entry.config(state="disabled"), recButton.config(state="disabled"), textVar.set("Enter X coordinate random distribution range (in pixels):"), fromEntry.config(state="normal"), toEntry.config(state="normal"), setFromRange()))
    
    if value != None:
        if isinstance(value, int):
            var.set(str(value))
        else:
            from ..objects.fuzz_utils import randrange
            if isinstance(value, randrange):
                mode.set("range")
                fromVar.set(str(value.start))
                toVar.set(str(value.stop))

    def place():
        text.place(x = 0, y = 0)
        fixedModeButton.place(x = 0, y = text.winfo_height() + 5)
        recButton.place(x = master.winfo_width(), y = text.winfo_height() + 5, width = 75, height = fixedModeButton.winfo_height(), anchor = "ne")
        entry.place(x = fixedModeButton.winfo_width(), y = text.winfo_height() + 5, width = master.winfo_width() - fixedModeButton.winfo_width() - recButton.winfo_width(), height = fixedModeButton.winfo_height())
        rangeModeButton.place(x = 0, y = fixedModeButton.winfo_y() + fixedModeButton.winfo_height() + 5)
        fromText.place(x = rangeModeButton.winfo_width() + 5, y = rangeModeButton.winfo_y(), width = 30, height = rangeModeButton.winfo_height())
        fromEntry.place(x = fromText.winfo_x() + fromText.winfo_width() + 5, y = fromText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())
        toText.place(x = fromEntry.winfo_x() + fromEntry.winfo_width() + 5, y = fromEntry.winfo_y(), width = 15, height = rangeModeButton.winfo_height())
        toEntry.place(x = toText.winfo_x() + toText.winfo_width() + 5, y = toText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())

    return trueVar, place


def _y_select(master, value = None) -> Tuple[tk.Variable, Callable]:

    from .var import freeVar

    trueVar = freeVar()

    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = int(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def recordClick(*args):
        from pynput import mouse
        coor = [None]
        def listen(x, y, button, pressed):
            coor[0] = y
            return False
        with mouse.Listener(on_click = listen) as listener:
            listener.join()
        var.set(str(coor[0]))
    def setFromFixed(*args):
        trueVar.set(int(var.get() or "0"))
    def setFromRange(*args):
        from ..objects.fuzz_utils import randrange
        a, b = fromVar.get(), toVar.get()
        if not a:
            return
        if not b:
            return
        a, b = int(a), int(b)
        if a > b:
            b, a = a, b
        trueVar.set(randrange(a, b))

    textVar = tk.Variable(master, value="Enter Y coordinate (in pixels):")
    text = tk.Label(master, textvariable=textVar)

    var = tk.Variable(master)
    var.trace_add("write", setFromFixed)
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    recButton = tk.Button(master, text="Record", command=recordClick)

    fromText = tk.Label(master, text="From")
    fromVar = tk.Variable(master)
    fromVar.trace_add("write", setFromRange)
    toText = tk.Label(master, text="to")
    toVar = tk.Variable(master)
    toVar.trace_add("write", setFromRange)
    fromEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=fromVar, state="disabled")
    toEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=toVar, state="disabled")

    mode = tk.Variable(master, value="fixed")
    fixedModeButton = tk.Radiobutton(master, value="fixed", variable=mode, command=lambda *args: (entry.config(state="normal"), recButton.config(state="normal"), textVar.set("Enter Y coordinate (in pixels):"), fromEntry.config(state="disabled"), toEntry.config(state="disabled"), setFromFixed()))
    rangeModeButton = tk.Radiobutton(master, value="range", variable=mode, command=lambda *args: (entry.config(state="disabled"), recButton.config(state="disabled"), textVar.set("Enter Y coordinate random distribution range (in pixels):"), fromEntry.config(state="normal"), toEntry.config(state="normal"), setFromRange()))
    
    fromVar.set(0)
    toVar.set(20)
    var.set("0")

    if value != None:
        if isinstance(value, int):
            var.set(str(value))
        else:
            from ..objects.fuzz_utils import randrange
            if isinstance(value, randrange):
                mode.set("range")
                fromVar.set(str(value.start))
                toVar.set(str(value.stop))

    def place():
        text.place(x = 0, y = 0)
        fixedModeButton.place(x = 0, y = text.winfo_height() + 5)
        recButton.place(x = master.winfo_width(), y = text.winfo_height() + 5, width = 75, height = fixedModeButton.winfo_height(), anchor = "ne")
        entry.place(x = fixedModeButton.winfo_width(), y = text.winfo_height() + 5, width = master.winfo_width() - fixedModeButton.winfo_width() - recButton.winfo_width(), height = fixedModeButton.winfo_height())
        rangeModeButton.place(x = 0, y = fixedModeButton.winfo_y() + fixedModeButton.winfo_height() + 5)
        fromText.place(x = rangeModeButton.winfo_width() + 5, y = rangeModeButton.winfo_y(), width = 30, height = rangeModeButton.winfo_height())
        fromEntry.place(x = fromText.winfo_x() + fromText.winfo_width() + 5, y = fromText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())
        toText.place(x = fromEntry.winfo_x() + fromEntry.winfo_width() + 5, y = fromEntry.winfo_y(), width = 15, height = rangeModeButton.winfo_height())
        toEntry.place(x = toText.winfo_x() + toText.winfo_width() + 5, y = toText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())

    return trueVar, place


def _dx_select(master, value = None) -> Tuple[tk.Variable, Callable]:

    from .var import freeVar

    trueVar = freeVar()

    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = int(entry)
        except:
            return False
        return True
    def setFromFixed(*args):
        trueVar.set(int(var.get() or "0"))
    def setFromRange(*args):
        from ..objects.fuzz_utils import randrange
        a, b = fromVar.get(), toVar.get()
        if not a:
            return
        if not b:
            return
        a, b = int(a), int(b)
        if a > b:
            b, a = a, b
        trueVar.set(randrange(a, b))

    textVar = tk.Variable(master, value="Enter X displacement (in pixels):")
    text = tk.Label(master, textvariable=textVar)

    var = tk.Variable(master)
    var.trace_add("write", setFromFixed)
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)

    fromText = tk.Label(master, text="From")
    fromVar = tk.Variable(master)
    fromVar.trace_add("write", setFromRange)
    toText = tk.Label(master, text="to")
    toVar = tk.Variable(master)
    toVar.trace_add("write", setFromRange)
    fromEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=fromVar, state="disabled")
    toEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=toVar, state="disabled")

    mode = tk.Variable(master, value="fixed")
    fixedModeButton = tk.Radiobutton(master, value="fixed", variable=mode, command=lambda *args: (entry.config(state="normal"), textVar.set("Enter X displacement (in pixels):"), fromEntry.config(state="disabled"), toEntry.config(state="disabled"), setFromFixed()))
    rangeModeButton = tk.Radiobutton(master, value="range", variable=mode, command=lambda *args: (entry.config(state="disabled"), textVar.set("Enter X displacement random distribution range (in pixels):"), fromEntry.config(state="normal"), toEntry.config(state="normal"), setFromRange()))
    
    fromVar.set(0)
    toVar.set(20)
    var.set("0")

    if value != None:
        if isinstance(value, int):
            var.set(str(value))
        else:
            from ..objects.fuzz_utils import randrange
            if isinstance(value, randrange):
                mode.set("range")
                fromVar.set(str(value.start))
                toVar.set(str(value.stop))

    def place():
        text.place(x = 0, y = 0)
        fixedModeButton.place(x = 0, y = text.winfo_height() + 5)
        entry.place(x = fixedModeButton.winfo_width(), y = text.winfo_height() + 5, width = master.winfo_width() - fixedModeButton.winfo_width(), height = fixedModeButton.winfo_height())
        rangeModeButton.place(x = 0, y = fixedModeButton.winfo_y() + fixedModeButton.winfo_height() + 5)
        fromText.place(x = rangeModeButton.winfo_width() + 5, y = rangeModeButton.winfo_y(), width = 30, height = rangeModeButton.winfo_height())
        fromEntry.place(x = fromText.winfo_x() + fromText.winfo_width() + 5, y = fromText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())
        toText.place(x = fromEntry.winfo_x() + fromEntry.winfo_width() + 5, y = fromEntry.winfo_y(), width = 15, height = rangeModeButton.winfo_height())
        toEntry.place(x = toText.winfo_x() + toText.winfo_width() + 5, y = toText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())

    return trueVar, place


def _dy_select(master, value = None) -> Tuple[tk.Variable, Callable]:

    from .var import freeVar

    trueVar = freeVar()

    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = int(entry)
        except:
            return False
        return True
    def setFromFixed(*args):
        trueVar.set(int(var.get() or "0"))
    def setFromRange(*args):
        from ..objects.fuzz_utils import randrange
        a, b = fromVar.get(), toVar.get()
        if not a:
            return
        if not b:
            return
        a, b = int(a), int(b)
        if a > b:
            b, a = a, b
        trueVar.set(randrange(a, b))

    textVar = tk.Variable(master, value="Enter Y displacement (in pixels):")
    text = tk.Label(master, textvariable=textVar)

    var = tk.Variable(master)
    var.trace_add("write", setFromFixed)
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)

    fromText = tk.Label(master, text="From")
    fromVar = tk.Variable(master)
    fromVar.trace_add("write", setFromRange)
    toText = tk.Label(master, text="to")
    toVar = tk.Variable(master)
    toVar.trace_add("write", setFromRange)
    fromEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=fromVar, state="disabled")
    toEntry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=toVar, state="disabled")

    mode = tk.Variable(master, value="fixed")
    fixedModeButton = tk.Radiobutton(master, value="fixed", variable=mode, command=lambda *args: (entry.config(state="normal"), textVar.set("Enter Y displacement (in pixels):"), fromEntry.config(state="disabled"), toEntry.config(state="disabled"), setFromFixed()))
    rangeModeButton = tk.Radiobutton(master, value="range", variable=mode, command=lambda *args: (entry.config(state="disabled"), textVar.set("Enter Y displacement random distribution range (in pixels):"), fromEntry.config(state="normal"), toEntry.config(state="normal"), setFromRange()))
    
    fromVar.set(0)
    toVar.set(20)
    var.set("0")

    if value != None:
        if isinstance(value, int):
            var.set(str(value))
        else:
            from ..objects.fuzz_utils import randrange
            if isinstance(value, randrange):
                mode.set("range")
                fromVar.set(str(value.start))
                toVar.set(str(value.stop))

    def place():
        text.place(x = 0, y = 0)
        fixedModeButton.place(x = 0, y = text.winfo_height() + 5)
        entry.place(x = fixedModeButton.winfo_width(), y = text.winfo_height() + 5, width = master.winfo_width() - fixedModeButton.winfo_width(), height = fixedModeButton.winfo_height())
        rangeModeButton.place(x = 0, y = fixedModeButton.winfo_y() + fixedModeButton.winfo_height() + 5)
        fromText.place(x = rangeModeButton.winfo_width() + 5, y = rangeModeButton.winfo_y(), width = 30, height = rangeModeButton.winfo_height())
        fromEntry.place(x = fromText.winfo_x() + fromText.winfo_width() + 5, y = fromText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())
        toText.place(x = fromEntry.winfo_x() + fromEntry.winfo_width() + 5, y = fromEntry.winfo_y(), width = 15, height = rangeModeButton.winfo_height())
        toEntry.place(x = toText.winfo_x() + toText.winfo_width() + 5, y = toText.winfo_y(), width = 40, height = rangeModeButton.winfo_height())

    return trueVar, place


def _factor_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = float(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def convert(*args):
        entry = var.get() or "0"
        trueVar.set(float(entry))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("2")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter dilation factor (ex: 1 for unchanged, 2 for double):")
    if value != None:
        var.set(str(value))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
    return trueVar, place


def _variance_select(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = float(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def convert(*args):
        entry = var.get() or "0"
        trueVar.set(float(entry))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("0.3")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter variance V for the law 𝒩(1.0, V)\nthat will be used to generate dilation factors:")
    if value != None:
        var.set(str(value))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
    return trueVar, place


def _select_function(master, value = None) -> Tuple[tk.Variable, Callable]:
    text = tk.Label(master, text="No request interface available for argument 'function'.\nUse the Python interpreter to set up a custom transform.")
    def place():
        text.place(x = 0, y = 0)
    return tk.Variable(master, value=None), place


def _select_classes(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    var = freeVar(set())

    from ..objects import events
    eventClasses = {"Event" : events.Event}
    current = tk.Variable()

    for name in dir(events):
        if isinstance(getattr(events, name), type) and issubclass(getattr(events, name), events.Event):
            eventClasses[name] = getattr(events, name)

    selector = ttk.Combobox(master, values=list(eventClasses), state="readonly", textvariable=current)
    eventList = tk.Listbox(master, selectmode="single")
    eventScroll = tk.Scrollbar(master, command=eventList.yview)
    eventList['yscrollcommand'] = eventScroll.set

    def update():
        eventList.delete(0, eventList.size())
        for eventClass in var.get():
            eventList.insert("end", eventClass.__name__)
    var.trace_add("write", update)

    def add(*args):
        s = var.get()
        s.add(eventClasses[current.get()])
        var.set(s)
    def remove(*args):
        s = var.get()
        s.discard(eventClasses[current.get()])
        var.set(s)
    addButton = tk.Button(master, text="Add", command=add)
    removeButton = tk.Button(master, text="Remove", command=remove)
    
    def place():
        selector.place(x = 0, y = 0, width = 150)
        addButton.place(x = 0, y = selector.winfo_height(), width = 75)
        removeButton.place(x = 75, y = selector.winfo_height(), width = 75)
        eventList.place(x = 150, y = 0, width = master.winfo_width() - 150 - eventScroll.winfo_width(), height = selector.winfo_height() + addButton.winfo_height())
        eventScroll.place(x = eventList.winfo_x() + eventList.winfo_width() - 2, y = 0, height = selector.winfo_height() + addButton.winfo_height())
    
    return var, place


def _select_inverse(master, value = None) -> Tuple[tk.Variable, Callable]:
    var = tk.BooleanVar(master, value=False)
    button = tk.Checkbutton(master, text="Inverse", variable=var)
    text = tk.Label(master, text="")

    def update(*args):
        if var.get():
            text.config(text="All matching events will be deleted.")
        else:
            text.config(text="All matching events will be kept.")
    var.trace_add("write", update)
    update()

    def place():
        text.place(x = 0, y = 0)
        button.place(x = 0, y = text.winfo_height())
    
    return var, place
    


def _select_empty(master, value = None) -> Tuple[tk.Variable, Callable]:
    def place():
        pass
    return tk.Variable(master, value=None), place


def _select_default(master, value = None) -> Tuple[tk.Variable, Callable]:
    text = tk.Label(master, text="No request interface available for argument " + str(value))
    def place():
        text.place(x = 0, y = 0)
    return tk.Variable(master, value=None), place


def _select_inactivity(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = float(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def convert(*args):
        entry = var.get()
        trueVar.set(round(float(entry) * 1000000000))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("0.5")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter inactivity duration (in seconds):")
    text2 = tk.Label(master, wraplength=325, anchor="nw", justify="left", text="After this duration without moving, the mouse will be marked as idle.")
    if value != None:
        var.set(str(value / 1000000000))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
        text2.place(x = 0, y = entry.winfo_y() + entry.winfo_height(), width = master.winfo_width())
    return trueVar, place


def _select_max_duration(master, value = None) -> Tuple[tk.Variable, Callable]:
    from .var import freeVar
    def validate(entry) -> bool:
        if entry == "":
            return True
        try:
            n = float(entry)
        except:
            return False
        if n < 0:
            return False
        return True
    def convert(*args):
        entry = var.get()
        trueVar.set(round(float(entry) * 1000000000))
    trueVar = freeVar()
    var = tk.Variable(master)
    var.trace_add("write", convert)
    var.set("2.0")
    entry = tk.Entry(master, validate="all", validatecommand=(master.register(validate), '%P'), textvariable=var)
    text = tk.Label(master, text="Enter maximum move duration (in seconds):")
    text2 = tk.Label(master, wraplength=325, anchor="nw", justify="left", text="After this duration of moving, an intermadiate point will be added to the mouse path.")
    if value != None:
        var.set(str(value / 1000000000))
    def place():
        text.place(x = 0, y = 0)
        entry.place(x = 0, y = text.winfo_height() + 5, width = text.winfo_width())
        text2.place(x = 0, y = entry.winfo_y() + entry.winfo_height(), width = master.winfo_width())
    return trueVar, place



_selection_table = {
    "time" : _time_select, 
    "key" : _key_select,
    "x" : _x_select,
    "y" : _y_select,
    "dx" : _dx_select,
    "dy" : _dy_select,
    "dt" : _time_select,
    "input" : _input_select,
    "speed" : _speed_select,
    "times" : _times_select,
    "button" : _button_select,
    "factor" : _factor_select,
    "variance" : _variance_select,
    "function" : _select_function,
    "classes" : _select_classes,
    "args" : _select_empty,
    "kwargs" : _select_empty,
    "inverse" : _select_inverse,
    "inactivity" : _select_inactivity,
    "max_duration" : _select_max_duration,
    "default" : _select_default
}