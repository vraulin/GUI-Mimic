import tkinter as tk
from source.gui.var import freeVar

from source.objects.events import Event


class EventManager(tk.Toplevel):

    def link(self, event : freeVar):
        from ..objects import events
        self.eventClasses = {"Event" : events.Event}
        self.eventVar = event
        event = event.get()
        self.placers = []
        self.reshaping = False
        for name in dir(events):
            if isinstance(getattr(events, name), type) and issubclass(getattr(events, name), events.Event):
                self.eventClasses[name] = getattr(events, name)
        from tkinter import ttk
        self.eventPreviousType = True.__class__
        self.eventType = None.__class__
        self.currentType = tk.Variable(self)
        self.typeSelector = ttk.Combobox(self, values=list(self.eventClasses), state="readonly", textvariable=self.currentType)
        self.typeSelector.bind("<<ComboboxSelected>>", self.place_parameters)
        self.parameterDict = {}
        self.parameterFrames = []
        self.okButton = tk.Button(self, text="OK", command=self.create_event)
        self.cancelButton = tk.Button(self, text="Cancel", command=self.destroy)

        self.iconbitmap("source/images/logo.ico")
        self.title("Event customization")

        if event:
            self.typeSelector.set(event.__class__.__name__)
        else:
            self.typeSelector.set("Event")

        self.refresh()

        self.bind("<Configure>", self.refresh)
        self.minsize(350, 300)

        from sys import platform
        if "win" in platform.lower():
            self.master.attributes("-disabled", True)

        self.place_parameters()


    def create_event(self, event = None) -> Event:
        try:
            from .selectors import _listing_table
            d = {name : value.get() for name, value in self.parameterDict.items() if name not in _listing_table}
            l = []
            for name, value in self.parameterDict.items():
                if name in _listing_table:
                    l.extend(value.get())
            event = self.eventClasses[self.currentType.get()](*l, **d)
        except:
            from tkinter import messagebox
            if not self.parameterDict:
                messagebox.showerror(message="Cannot create pure abstract event.")
            else:
                messagebox.showerror(message="Event is missing some parameters.")
            return
        self.eventVar.set(event)
        self.destroy()


    def destroy(self) -> None:
        from sys import platform
        if "win" in platform.lower():
            self.master.attributes("-disabled", False)
        self.master.rebind()
        return super().destroy()


    def place_parameters(self, event = None):
        t = self.currentType.get()
        if t:
            self.eventType = self.eventClasses[t]
        else:
            self.eventType = None.__class__
        

        if self.eventType != self.eventPreviousType:
            for frame in self.parameterFrames:
                frame.destroy()
            self.parameterFrames.clear()
            self.parameterDict.clear()
            self.placers.clear()
            eventType = self.eventClasses[self.currentType.get()]
            event = self.eventVar.get()
            from .selectors import _name_translation_table, _selection_table
            for argument in eventType.__init__.__annotations__:
                if argument != "return":
                    default = None
                    if event != None and hasattr(event, argument):
                        default = getattr(event, argument)
                    nameFrame = tk.Frame(self, borderwidth=1, relief="raised")
                    name = tk.Label(nameFrame, text=(argument.title() if argument not in _name_translation_table else _name_translation_table[argument]))
                    name.place(x = 0, y = 0)
                    self.parameterFrames.append(nameFrame)
                    frame = tk.Frame(self, borderwidth=1, relief="sunken")
                    self.parameterFrames.append(frame)
                    if argument in _selection_table:
                        param, placer = _selection_table[argument](frame, default)
                    else:
                        param, placer = _selection_table["default"](frame, argument)
                    self.parameterDict[argument] = param
                    placer()
                    self.placers.append(placer)
                    frame.update()
                    nameFrame.update()
            self.eventPreviousType = self.eventType

        self.refresh(20)

    
    def refresh(self, times : int = 3):

        if self.reshaping:
            return
        self.reshaping = True

        if not isinstance(times, int):
            times = 3
        
            
        for i in range(times):
            try:
                self.typeSelector.place(x = 0, y = 0, width = self.winfo_width())

                lasty = self.typeSelector.winfo_height()
                for i, frame in enumerate(self.parameterFrames):
                    frame.place(x = 0, y = lasty + (1 - i%2) * 20, relwidth = 1.0, height = max(child.winfo_y() + child.winfo_height() for child in frame.children.values()) + 2)
                    lasty = frame.winfo_y() + frame.winfo_height()

                for placer in self.placers:
                    placer()

                self.okButton.place(relx = 1 / 3, rely = 1.0, width = 75, anchor = "s")
                self.cancelButton.place(relx = 2 / 3, rely = 1.0, width = 75, anchor = "s")

                self.minsize(350, lasty + self.okButton.winfo_height())
            except:
                pass
        

        self.reshaping = False