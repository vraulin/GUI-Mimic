import tkinter as tk
from traceback import print_exc

from source.objects.transforms import Transform

from ..objects.events import Event, KeyboardEvent, MouseEvent
from ..objects.user_guide import UserGuide


def convertEventName(event : Event) -> str:
    name = event.__class__.__name__
    if "Event" in name:
        i = name.index("Event")
        name = name[:i] + name[i + 5:]
    i = len(name)
    for s in reversed(name):
        i -= 1
        if s.isupper():
            name = name[:i] + " " + name[i:]
    while name[0:1] == "":
        name = name[1:]
    return name


def convertTransformName(transform : Transform) -> str:
    name = transform.__class__.__name__.replace("_", " ")
    name = name[0:1].upper() + name[1:]
    return name



class SequenceManager(tk.Toplevel):

    def link(self, guide : UserGuide, seq : str):
        self.title("Sequence : " + seq)
        self.name = seq
        self.sequence = getattr(guide, seq)
        self.guide = guide
        from ..objects.user_sequence import UserSequence
        if not isinstance(self.sequence, UserSequence):
            from tkinter import messagebox
            messagebox.showerror(message="An object already has this name and is not a sequence")
            self.destroy()
        
        import tkinter as tk

        self.eventList = tk.Listbox(self, selectmode="single")
        self.eventList.bind("<Double-Button-1>", self.edit)
        self.eventScroll = tk.Scrollbar(self, command=self.eventList.yview)
        self.eventList['yscrollcommand'] = self.eventScroll.set
        

        self.sequenceLabel = tk.Label(self, text="Sequence name:")
        self.sequenceName = tk.Variable(self, value=seq)
        self.sequenceNameZone = tk.Entry(self, textvariable=self.sequenceName)
        
        self.insertButton = tk.Button(self, text="Insert", command=self.insert)
        self.editButton = tk.Button(self, text="Edit", command=self.edit)
        self.deleteButton = tk.Button(self, text="Delete", command=self.delete)

        self.infoZone = tk.Label(self, text="Some information")

        self.applyTransformButton = tk.Button(self, text="Apply transform", command=self.applyTransform)
        self.scheduleTransformButton = tk.Button(self, text="Schedule transform", command=self.scheduleTransform)

        self.transformText = tk.Label(self, text="Runtime transforms:")

        self.transformList = tk.Listbox(self, selectmode="single")
        self.transformList.bind("<Double-Button-1>", self.editTransform)
        self.transformScroll = tk.Scrollbar(self, command=self.transformList.yview)
        self.transformList["yscrollcommand"] = self.transformScroll.set

        self.deleteTransformButton = tk.Button(self, text="Delete", command=self.deleteTransform)
        self.MoveUpTransformButton = tk.Button(self, text="Move up", command=self.moveUpTransform)
        self.MoveDownTransformButton = tk.Button(self, text="Move down", command=self.moveDownTransform)

        self.iconbitmap("source/images/logo.ico")

        self.refresh()
        self.rebind()
        self.refresh()


    def rebind(self):
        
        self.sequenceNameZone.bind("<FocusOut>", self.updateName)
        self.sequenceNameZone.bind("<Return>", self.updateName)

        self.bind("<Configure>", self.refresh)
        self.minsize(500, 300)
    

    def current(self) -> int:
        current = self.eventList.curselection()
        if current:
            return current[0]
        return -1
    

    def currentTransform(self) -> int:
        current = self.transformList.curselection()
        if current:
            return current[0]
        return -1
    

    def getEntry(self) -> str:
        try:
            pass
        except:
            return ""
        return self.entryZone.get()
    

    def insert(self, event = None):
        from .event import EventManager
        from .var import freeVar
        win = EventManager(self)
        var = freeVar(None)
        current = self.current()
        if current < 0:
            current = len(self.sequence)
        def insertor():
            self.sequence._sequence.insert(current, var.get())
            self.master.change()
            self.refresh()
            self.rebind()
            self.refresh()
        var.trace_add("write", insertor)
        win.link(var)


    def edit(self, event = None):
        from .event import EventManager
        from .var import freeVar
        current = self.current()
        if current < 0:
            return
        win = EventManager(self)
        var = freeVar(self.sequence[current])
        def editor():
            self.sequence._sequence[current] = var.get()
            self.master.change()
            self.refresh()
            self.rebind()
            self.refresh()
        var.trace_add("write", editor)
        win.link(var)


    def delete(self, event = None):
        current = self.current()
        if current < 0:
            return
        self.sequence._sequence.pop(current)
        self.master.change()
        self.refresh()

    
    def applyTransform(self, event = None):
        from .transform import TransformManager
        from .var import freeVar
        win = TransformManager(self)
        var = freeVar()
        def apply():
            t = var.get()
            seq = self.sequence.apply_transform(t)
            seq._atRunTransforms = self.sequence._atRunTransforms
            self.sequence = seq
            setattr(self.guide, self.name, self.sequence)
            self.master.change()
            self.refresh()
            self.rebind()
            self.refresh()
        var.trace_add("write", apply)
        win.link(var)


    def scheduleTransform(self, event = None):
        from .transform import TransformManager
        from .var import freeVar
        win = TransformManager(self)
        var = freeVar()
        def schedule():
            t = var.get()
            self.sequence.schedule_transform(t)
            self.master.change()
            self.refresh()
            self.rebind()
            self.refresh()
        var.trace_add("write", schedule)
        win.link(var)


    def editTransform(self, event = None):
        from .transform import TransformManager
        from .var import freeVar
        current = self.currentTransform()
        if current < 0:
            return
        win = TransformManager(self)
        var = freeVar(self.sequence._atRunTransforms[current])
        def edit():
            t = var.get()
            self.sequence._atRunTransforms[current] = t
            self.master.change()
            self.refresh()
            self.rebind()
            self.refresh()
        var.trace_add("write", edit)
        win.link(var)


    def deleteTransform(self, event = None):
        current = self.currentTransform()
        if current < 0:
            return
        self.sequence._atRunTransforms.pop(current)
        self.master.change()
        self.refresh()
        self.rebind()
        self.refresh()


    def moveUpTransform(self, event = None):
        current = self.currentTransform()
        if current <= 0:
            return
        t1, t2 = self.sequence._atRunTransforms[current - 1: current + 1]
        self.sequence._atRunTransforms[current - 1: current + 1] = [t2, t1]
        self.master.change()
        self.refresh()
        self.rebind()
        self.refresh()


    def moveDownTransform(self, event = None):
        current = self.currentTransform()
        if current < 0 or current == len(self.sequence._atRunTransforms) - 1:
            return
        t1, t2 = self.sequence._atRunTransforms[current: current + 2]
        self.sequence._atRunTransforms[current: current + 2] = [t2, t1]
        self.master.change()
        self.refresh()
        self.rebind()
        self.refresh()
    

    def updateName(self, event = None):
        new_name = self.sequenceName.get()
        if hasattr(self.guide, new_name) and new_name != self.name:
            from tkinter import messagebox
            messagebox.showerror(message="The new sequence name already exists for another sequence or scenario")
            self.sequenceNameZone.focus_set()
            return
        if new_name != self.name:
            setattr(self.guide, new_name, self.sequence)
            delattr(self.guide, self.name)
            self.master.sequenceWindows.pop(self.name)
            self.master.sequenceWindows[new_name] = self
            self.name = new_name
            self.title("Sequence : " + self.name)
            self.master.change()
            self.master.refresh()
    

    def refresh(self, times = 3):
        from ..format import duration
        if not isinstance(times, int):
            times = 3
        current = self.current()
        for i in range(times):
            try:

                self.sequenceLabel.place(x = 0, y = 0, height = 20)
                self.sequenceNameZone.place(x = self.sequenceLabel.winfo_width() + 5, y = 0, height = 20)

                self.eventList.delete(0, self.eventList.size())
                for entry in self.sequence._sequence:
                    self.eventList.insert("end", convertEventName(entry))
                
                self.eventList.place(x = 0, y = 25, width = self.winfo_width() / 2 - self.eventScroll.winfo_width(), height = self.winfo_height() - self.eventList.winfo_y())
                # self.eventList.see(current)
                self.eventScroll.place(x = self.eventList.winfo_width(), y = self.eventList.winfo_y(), height = self.winfo_height() - self.eventList.winfo_y())

                self.insertButton.place(x = self.winfo_width() / 2 + 10, y = 30, width = (self.winfo_width() / 2 - 20) / 3)
                self.editButton.place(x = self.insertButton.winfo_x() + self.insertButton.winfo_width(), y = self.insertButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)
                self.deleteButton.place(x = self.editButton.winfo_x() + self.editButton.winfo_width(), y = self.insertButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)

                self.infoZone.place(x = self.insertButton.winfo_x(), y = self.insertButton.winfo_y() + self.insertButton.winfo_height() + 10, width = self.winfo_width() / 2 - 20, height = 60)
                self.infoZone.config(text="{} events\n{} mouse events, {} keyboard events\n{} base duration\n{} transformations".format(len(self.sequence), len(list(e for e in self.sequence if isinstance(e, MouseEvent))), len(list(e for e in self.sequence if isinstance(e, KeyboardEvent))), duration(self.sequence.duration), len(self.sequence._atRunTransforms)))

                self.applyTransformButton.place(x = self.insertButton.winfo_x(), y = self.infoZone.winfo_y() + self.infoZone.winfo_height(), width = (self.winfo_width() / 2 - 20) / 2)
                self.scheduleTransformButton.place(x = self.applyTransformButton.winfo_x() + self.applyTransformButton.winfo_width(), y = self.applyTransformButton.winfo_y(), width = self.applyTransformButton.winfo_width())

                self.transformText.place(x = self.applyTransformButton.winfo_x(), y = self.applyTransformButton.winfo_y() + self.applyTransformButton.winfo_height() + 10, width = self.winfo_width() / 2 - 20, height = 20)

                self.transformList.delete(0, self.transformList.size())
                for entry in self.sequence._atRunTransforms:
                    self.transformList.insert("end", convertTransformName(entry))
                    
                self.transformList.place(x = self.insertButton.winfo_x(), y = self.transformText.winfo_y() + self.transformText.winfo_height(), width = self.winfo_width() / 2 - 20 - self.transformScroll.winfo_width(), height = self.winfo_height() - self.transformList.winfo_y() - self.deleteTransformButton.winfo_height())
                self.transformScroll.place(x = self.transformList.winfo_x() + self.transformList.winfo_width(), y = self.transformList.winfo_y(), height = self.transformList.winfo_height())

                self.deleteTransformButton.place(x = self.transformList.winfo_x(), y = self.transformList.winfo_y() + self.transformList.winfo_height(), width = (self.winfo_width() / 2 - 20) / 3)
                self.MoveUpTransformButton.place(x = self.deleteTransformButton.winfo_x() + self.deleteTransformButton.winfo_width(), y = self.deleteTransformButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)
                self.MoveDownTransformButton.place(x = self.MoveUpTransformButton.winfo_x() + self.MoveUpTransformButton.winfo_width(), y = self.MoveUpTransformButton.winfo_y(), width = (self.winfo_width() / 2 - 20) / 3)

            except:
                pass


    def destroy(self) -> None:
        self.updateName()
        self.master.sequenceWindows.pop(self.name)
        return super().destroy()