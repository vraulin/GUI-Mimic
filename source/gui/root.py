import tkinter as tk
from typing import Any
from traceback import print_exc




class GUIInterface(tk.Tk):

    def __init__(self) -> None:
        super().__init__()
        import tkinter as tk
        from tkinter import PhotoImage
        from ..objects.user_guide import UserGuide
        from ..objects import recorder

        self.project = None         # Active project file
        self.active_guide = UserGuide()
        self.saved = True           # Changes to be saved?

        self.scenarioWindows = {}
        self.sequenceWindows = {}

        ## Build the interface. From top to bottom

        self.update_name()

        ## Menu bar:

        self.menubar = tk.Menu(self)
        
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="New", command=self.new)
        self.filemenu.add_command(label="Open", command=self.open)
        self.filemenu.add_command(label="Save", command=self.save)
        self.filemenu.add_command(label="Save as", command=self.save_as)
        self.auto_save = tk.BooleanVar(self, value=False)
        self.filemenu.add_separator()
        self.filemenu.add_checkbutton(label="Auto-save", variable=self.auto_save)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.config(menu=self.menubar)

        ## Recording and playback

        self.recordingFrame = tk.Frame(self, borderwidth=2, relief="sunken")

        self.recordNameLabel = tk.Label(self.recordingFrame, text="Name sequence as")
        self.currentRecord = tk.Variable(self.recordingFrame)
        self.recordName = tk.Entry(self.recordingFrame, textvariable=self.currentRecord)
        self.autoRefine = tk.BooleanVar(self.recordingFrame, value=True)
        self.autoRefineButton = tk.Checkbutton(self.recordingFrame, text="Synthesize sequence", variable=self.autoRefine)
        self.recording = False
        self.playing = False
        self.recordImage = PhotoImage(file="source/images/record.png")
        self.stopImage = PhotoImage(file="source/images/stop.png")
        self.playImage = PhotoImage(file="source/images/play.png")
        self.recordingButton = tk.Button(self.recordingFrame, command=self.startStopRecording, image=self.recordImage, borderwidth=0)
        self.actionText = tk.Label(self, text="")

        self.importImage = PhotoImage(file="source/images/import.png")

        self.infoZone = tk.Label(self, text="")

        ## Scenarios

        self.scenarioFrame = tk.Frame(self, borderwidth=1, relief="sunken")
        self.scenarioToolBar = tk.Frame(self.scenarioFrame, borderwidth=1, relief="raised")

        self.scenarioLabel = tk.Label(self.scenarioToolBar, text="Scenario tools")
        self.scenarioNew = tk.Button(self.scenarioToolBar, text="New", font="TkMenuFont", borderwidth=1, command=self.newScenario)
        self.scenarioEdit = tk.Button(self.scenarioToolBar, text="Edit",font="TkMenuFont", borderwidth=1, command=self.editScenario)
        self.scenarioDelete = tk.Button(self.scenarioToolBar, text="Delete", font="TkMenuFont", borderwidth=1, command=self.deleteScenario)

        self.scenarioPlay = tk.Button(self.scenarioFrame, command=self.playScenario, image=self.playImage, borderwidth=0)
        self.scenarioPlayAndRecord = tk.Button(self.scenarioFrame, command=self.playAndRecordScenario, image=self.importImage, borderwidth=0)
        self.scenarioTextZone = tk.Label(self.scenarioFrame, text="")

        self.scenarioList = tk.Listbox(self.scenarioFrame, selectmode="single")
        self.scenarioList.bind("<Double-Button-1>", self.editScenario)
        self.scenarioScroll = tk.Scrollbar(self.scenarioFrame, command = self.scenarioList.yview)
        self.scenarioList['yscrollcommand'] = self.scenarioScroll.set

        ## Sequences

        self.sequenceFrame = tk.Frame(self, borderwidth=2, relief="sunken")
        self.sequenceToolBar = tk.Frame(self.sequenceFrame, borderwidth=1, relief="raised")

        self.sequenceLabel = tk.Label(self.sequenceToolBar, text="Sequence tools")
        self.sequenceNew = tk.Button(self.sequenceToolBar, text="New", font="TkMenuFont", borderwidth=1, command=self.newSequence)
        self.sequenceEdit = tk.Button(self.sequenceToolBar, text="Edit",font="TkMenuFont", borderwidth=1, command=self.editSequence)
        self.sequenceDelete = tk.Button(self.sequenceToolBar, text="Delete", font="TkMenuFont", borderwidth=1, command=self.deleteSequence)

        self.sequencePlay = tk.Button(self.sequenceFrame, command=self.playSequence, image=self.playImage, borderwidth=0)
        self.sequencePlayAndRecord = tk.Button(self.sequenceFrame, command=self.playAndRecordSequence, image=self.importImage, borderwidth=0)
        self.sequenceTextZone = tk.Label(self.sequenceFrame, text="")

        self.sequenceList = tk.Listbox(self.sequenceFrame, selectmode="single")
        self.sequenceList.bind("<Double-Button-1>", self.editSequence)
        self.sequenceScroll = tk.Scrollbar(self.sequenceFrame, command = self.sequenceList.yview)
        self.sequenceList['yscrollcommand'] = self.sequenceScroll.set
    
        ## Initialization
        
        from sys import platform
        if "win" in platform:
            self.iconbitmap("source/images/logo.ico")
        
        self.updateSequenceSummary()
        self.updateScenarioSummary()
        self.minsize(700, 300)
        self.refresh(10)
        self.bind('<Configure>', self.refresh)

        self.withMouse = False

        recorder.trace_add("start", self.startRecording)
        recorder.trace_add("stop", self.stopRecording)
    
    
    def updateSequenceSummary(self):
        from ..format import duration
        n = len(list(self.active_guide.sequences()))
        text = "{} sequences.".format(n)
        if n:
            total = sum(seq.duration for name, seq in self.active_guide.sequences())
            average = total / n
            minimum = min((seq.duration for name, seq in self.active_guide.sequences()), default=0)
            maximum = max((seq.duration for name, seq in self.active_guide.sequences()), default=0)
            text += "\n{} of recording.\nAverage: {}\nMinimum: {}\nMaximum: {}".format(duration(total), duration(average), duration(minimum), duration(maximum))
            nt = sum(len(seq._atRunTransforms) for name, seq in self.active_guide.sequences())
            if nt:
                nt /= n
                text += "\nScheduled transformations\nper sequence: {:.2f}".format(round(nt, 2))
        self.sequenceTextZone.config(text=text)
    

    def updateScenarioSummary(self):
        from ..format import duration
        import re
        n = len(list(self.active_guide.scenarios()))
        text = "{} scenarios.".format(n)
        if n:
            sequences = {name for name, seq in self.active_guide.sequences()}
            nseq = len(sequences)
            average = 0
            minimum = float("inf")
            maximum = 0
            for name, scenario in self.active_guide.scenarios():
                outcomes = []
                for expr in scenario:
                    matches = []
                    expr = re.compile(expr)
                    for seq_name, seq in self.active_guide.sequences():
                        if expr.fullmatch(seq_name):
                            sequences.discard(seq_name)
                            matches.append(seq)
                    outcomes.append(matches)
                size = 1
                for matches in outcomes:
                    size *= len(matches)
                if size >= 2 ** 10:
                    from random import choice
                    chosen_outcomes = set()
                    while len(chosen_outcomes) < 2 ** 10:
                        res = []
                        for matches in outcomes:
                            res.append(choice(matches))
                        chosen_outcomes.add(tuple(res))
                else:
                    from itertools import product
                    chosen_outcomes = set(product(*outcomes))
                if chosen_outcomes:
                    d1 = min((sum(seq.duration for seq in outcome) for outcome in chosen_outcomes), default=0)
                    d2 = max((sum(seq.duration for seq in outcome) for outcome in chosen_outcomes), default=0)
                    d3 = sum(sum(seq.duration for seq in outcome) for outcome in chosen_outcomes) / len(chosen_outcomes)
                    if d1 < minimum:
                        minimum = d1
                    if d2 > maximum:
                        maximum = d2
                    average += d3
            average /= n
            text += "\nOutcomes durations:\nMinimum: {}\nMaximum: {}\nAverage: {}".format(duration(minimum), duration(maximum), duration(average))
            if nseq:
                seq_coverage = (nseq - len(sequences)) / nseq * 100
                text += "\nSequences coverage: {}%".format(round(seq_coverage))
        self.scenarioTextZone.config(text=text)
    

    def startRecording(self, event = None):
        self.recordingButton.config(image=self.stopImage)
        self.infoZone.config(text="Recording", font=("Courier 30 bold"))
        self.refresh()
        self.recording = True
    

    def stopRecording(self, event = None):
        from ..objects.recorder import extract, get_recording_hotkeys
        from ..objects.events import MouseRelease, MousePress, KeyboardPress
        from ..objects.transforms import cleaning_procedure
        name = self.recordName.get()
        if not name:
            name = "sequence"
        sequences = {name for name, seq in self.active_guide.sequences()}
        if name in sequences:
            i = 1
            while name + "_" + str(i) in sequences:
                i += 1
            name += "_" + str(i)
        seq = extract()
        if self.withMouse:
            self.withMouse = False
            if seq and isinstance(seq[-1], MouseRelease):
                seq.pop(-1)
                for i in reversed(range(len(seq))):
                    if isinstance(seq[i], MousePress):
                        seq.pop(i)
                        break
        else:
            keys = get_recording_hotkeys()
            for i in reversed(range(len(seq))):
                if isinstance(seq[i], KeyboardPress) and seq[i].key in keys:
                    keys.remove(seq[i].key)
                    seq.pop(i)
        if self.autoRefine.get():
            for TransformClass in cleaning_procedure:
                seq = seq.apply_transform(TransformClass())
        setattr(self.active_guide, name, seq)
        self.recording = False
        self.recordingButton.config(image=self.recordImage)
        self.infoZone.config(text="", font=("Courier 30 bold"))
        self.change()
        self.refresh()


    def startStopRecording(self, event = None):
        from ..objects.recorder import start_listening, stop_listening
        if self.recording:
            self.withMouse = True
            stop_listening()
        else:
            start_listening()


    def change(self):
        self.saved = False
        self.update_name()
        self.updateSequenceSummary()
        self.updateScenarioSummary()
        if self.auto_save.get() and self.project:
            self.save()
        

    def destroy(self) -> None:
        if self.saved or self.ask_confirmation():
            return super().destroy()
    

    def ask_confirmation(self) -> bool:
        """
        Asks for confirmation before exiting/opening new file...in case of unsaved changes. Returns True if the action can be continued.
        """
        from tkinter import messagebox
        answer = messagebox.askyesnocancel(message="The current project has unsaved changes. Do you want to save them?")
        if answer == True:
            return self.save()
        elif answer == False:
            return True
        elif answer == None:
            return False
    

    def save(self) -> bool:
        """
        Tries to save the active project, asking the user if necessary. Returns True on success.
        """
        if self.project:
            from ..persistent import save
            try:
                save(self.project, self.active_guide)
                self.saved = True
                self.update_name()
                return True
            except:
                print_exc()
                from tkinter import messagebox
                messagebox.showerror(message="Could not save file.")
                return False
        else:
            return self.save_as()


    def save_as(self) -> bool:
        """
        Asks the user where to save the file and updates the interface with the new file location. Returns True on success.
        """
        from tkinter import filedialog
        path = filedialog.asksaveasfile(mode = "wb", defaultextension=".pyt", filetypes=[("Python Type", ".pyt")])
        if path:
            try:
                from ..persistent import save
                self.project = path.name
                save(path, self.active_guide)
                self.saved = True
                self.update_name()
                return True
            except:
                print_exc()
                from tkinter import messagebox
                messagebox.showerror(message="Could not save file.")
                return False
        return False


    def closeAll(self):
        for name, win in self.scenarioWindows.copy().items():
            win.destroy()
        for name, win in self.sequenceWindows.copy().items():
            win.destroy()


    def new(self) -> bool:
        """
        Tries to start a new project. Returns True on success.
        """
        if self.saved or self.ask_confirmation():
            from ..objects.user_guide import UserGuide
            self.closeAll()
            self.project = None
            self.active_guide = UserGuide()
            self.saved = True
            self.update_name()
            self.refresh()
    

    def open(self) -> bool:
        """
        Tries to open another file. Returns True on success.
        """
        if self.saved or self.ask_confirmation():
            from tkinter import filedialog
            path = filedialog.askopenfile(mode = "rb", filetypes=[("Python Type", ".pyt")])
            if path:
                try:
                    from ..persistent import load
                    obj = load(path)
                except:
                    print_exc()
                    from tkinter import messagebox
                    messagebox.showerror(message="The given file is corrupted.")
                    return False
                from ..objects.user_guide import UserGuide
                if not isinstance(obj, UserGuide):
                    return False
                self.closeAll()
                self.active_guide = obj
                self.project = path.name
                self.saved = True
                self.update_name()
                self.refresh()
        return False
                
    

    def update_name(self) -> str:
        from os.path import basename, splitext
        name = "GUI-Mimic "
        if self.project:
            name += "- "
        if not self.saved:
            name += "*"
        if self.project:
            name += splitext(basename(self.project))[0]
        self.title(name)
    

    def editScenario(self, event = None):
        current = self.scenarioList.curselection()
        if current:
            current = current[0]
            name = self.scenarioList.get(current)
            if name not in self.scenarioWindows:
                from .scenario import ScenarioManager
                win = ScenarioManager(self)
                win.link(self.active_guide, name)
                self.scenarioWindows[name] = win
            else:
                self.scenarioWindows[name].focus_set()
    

    def newScenario(self, event = None):
        from ..objects.user_scenario import UserScenario
        name = "scenario #" + str(self.scenarioList.size() + 1)
        setattr(self.active_guide, name, UserScenario())
        self.refresh()
        self.change()
    

    def deleteScenario(self, event = None):
        current = self.scenarioList.curselection()
        if current:
            current = current[0]
            name = self.scenarioList.get(current)
            if name in self.scenarioWindows:
                self.scenarioWindows[name].destroy()
            if hasattr(self.active_guide, name):
                delattr(self.active_guide, name)
                self.refresh()
                self.change()

    
    def playScenario(self, event = None):
        if not self.playing:
            from ..objects.recorder import lock_recording, unlock_recording
            from threading import Thread
            current = self.scenarioList.curselection()
            if current:
                current = current[0]
                name = self.scenarioList.get(current)
                sce = getattr(self.active_guide, name)
                def play():
                    self.infoZone.config(text="Playing", font=("Courier 30 bold"))
                    lock_recording()
                    sce()
                    unlock_recording()
                    self.infoZone.config(text="", font=("Courier 30 bold"))
                    self.playing = False
                t = Thread(target=play, daemon=True)
                self.playing = True
                t.start()
    

    def playAndRecordScenario(self, event = None):
        if not self.playing:
            from ..objects.recorder import lock_recording, unlock_recording
            from threading import Thread
            current = self.scenarioList.curselection()
            if current:
                current = current[0]
                name = self.scenarioList.get(current)
                sce = getattr(self.active_guide, name)
                def play():
                    self.recording = True
                    self.recordingButton.config(image=self.stopImage, state="disabled")
                    self.infoZone.config(text="Playing", font=("Courier 30 bold"))
                    lock_recording()
                    seq = sce()
                    unlock_recording()
                    self.infoZone.config(text="", font=("Courier 30 bold"))
                    name = self.recordName.get()
                    if not name:
                        name = "sequence"
                    sequences = {name for name, seq in self.active_guide.sequences()}
                    if name in sequences:
                        i = 1
                        while name + "_" + str(i) in sequences:
                            i += 1
                        name += "_" + str(i)
                    if self.autoRefine.get():
                        from ..objects.transforms import cleaning_procedure
                        for TransformClass in cleaning_procedure:
                            seq = seq.apply_transform(TransformClass())
                    setattr(self.active_guide, name, seq)
                    self.recording = False
                    self.recordingButton.config(image=self.recordImage, state="normal")  
                    self.change()
                    self.refresh()
                    self.playing = False
                t = Thread(target=play, daemon=True)
                self.playing = True
                t.start()
    

    def editSequence(self, event = None):
        current = self.sequenceList.curselection()
        if current:
            current = current[0]
            name = self.sequenceList.get(current)
            if name not in self.sequenceWindows:
                from .sequence import SequenceManager
                win = SequenceManager(self)
                win.link(self.active_guide, name)
                self.sequenceWindows[name] = win
            else:
                self.sequenceWindows[name].focus_set()
    

    def newSequence(self, event = None):
        from ..objects.user_sequence import UserSequence
        name = "sequence #" + str(self.sequenceList.size() + 1)
        setattr(self.active_guide, name, UserSequence())
        self.refresh()
        self.change()
    

    def deleteSequence(self, event = None):
        current = self.sequenceList.curselection()
        if current:
            current = current[0]
            name = self.sequenceList.get(current)
            if name in self.sequenceWindows:
                self.sequenceWindows[name].destroy()
            if hasattr(self.active_guide, name):
                delattr(self.active_guide, name)
                self.refresh()
                self.change()
    

    def playSequence(self, event = None):
        if not self.playing:
            from ..objects.recorder import lock_recording, unlock_recording
            from threading import Thread
            current = self.sequenceList.curselection()
            if current:
                current = current[0]
                name = self.sequenceList.get(current)
                sce = getattr(self.active_guide, name)
                def play():
                    self.infoZone.config(text="Playing", font=("Courier 30 bold"))
                    lock_recording()
                    sce()
                    unlock_recording()
                    self.infoZone.config(text="", font=("Courier 30 bold"))
                    self.playing = False
                t = Thread(target=play, daemon=True)
                self.playing = True
                t.start()
    

    def playAndRecordSequence(self, event = None):
        if not self.playing:
            from ..objects.recorder import lock_recording, unlock_recording
            from threading import Thread
            current = self.sequenceList.curselection()
            if current:
                current = current[0]
                name = self.sequenceList.get(current)
                sce = getattr(self.active_guide, name)
                def play():             
                    self.recording = True
                    self.recordingButton.config(image=self.stopImage, state="disabled")
                    lock_recording()
                    seq = sce()
                    unlock_recording()
                    name = self.recordName.get()
                    if not name:
                        name = "sequence"
                    sequences = {name for name, seq in self.active_guide.sequences()}
                    if name in sequences:
                        i = 1
                        while name + "_" + str(i) in sequences:
                            i += 1
                        name += "_" + str(i)
                    if self.autoRefine.get():
                        from ..objects.transforms import cleaning_procedure
                        for TransformClass in cleaning_procedure:
                            seq = seq.apply_transform(TransformClass())
                    setattr(self.active_guide, name, seq)
                    self.recording = False
                    self.recordingButton.config(image=self.recordImage, state="normal")
                    self.change()
                    self.refresh()
                    self.playing = False
                t = Thread(target=play, daemon=True)
                self.playing = True
                t.start()


    def refresh(self, times = 1):

        if not isinstance(times, int):
            times = 1
        for i in range(times):
            try:

                self.recordName.place(x=25, rely=0.5, height=25, width=200, anchor="w")
                self.recordNameLabel.place(x = 25, y = self.recordName.winfo_y(), anchor="sw")
                self.autoRefineButton.place(x = 25, y = self.recordName.winfo_y() + self.recordName.winfo_height(), height = 15)
                self.recordingButton.place(x=275, rely=0.5, width=50, height=50, anchor="center")

                self.scenarioToolBar.place(x = 1, y = 1, relwidth = self.scenarioFrame.winfo_width() - 2, height = 23)
                self.scenarioLabel.place(x = 0, rely = 0.5, height = 20, anchor = "w")
                self.scenarioDelete.place(x = self.scenarioFrame.winfo_width() - 4, y = 0, height=22, anchor="ne")
                self.scenarioEdit.place(x = self.scenarioDelete.winfo_x() - 1, y = 0, height=22, anchor="ne")
                self.scenarioNew.place(x = self.scenarioEdit.winfo_x() - 1, y = 0, height=22, anchor="ne")

                self.scenarioTextZone.place(x = self.scenarioFrame.winfo_width() / 4 * 3 + 12.5, y = 85 + (self.scenarioFrame.winfo_height() - 85) / 2, anchor = "center")

                current = self.scenarioList.curselection()
                if not current:
                    current = (0,)
                current = current[0]
                self.scenarioList.delete(0, self.scenarioList.size())
                for scenario_name, scenario in self.active_guide.scenarios():
                    self.scenarioList.insert("end", scenario_name)
                self.scenarioList.activate(current)
                self.scenarioList.place(x = 0, y = 25, height = self.scenarioFrame.winfo_height() - 27, relwidth = 0.5)
                self.scenarioScroll.place(x = self.scenarioList.winfo_width(), y = 25, height = self.scenarioFrame.winfo_height() - 27)

                self.scenarioPlay.place(x = self.scenarioFrame.winfo_width() / 2 + 10 + self.scenarioScroll.winfo_width(), y = 35, width = 50, height = 50)
                self.scenarioPlayAndRecord.place(x = self.scenarioFrame.winfo_width() - 11, y = 35, width = 50 , height = 50, anchor = "ne")


                self.sequenceToolBar.place(x = 1, y = 1, relwidth = self.sequenceFrame.winfo_width() - 2, height = 23)
                self.sequenceLabel.place(x = 0, rely = 0.5, height = 20, anchor = "w")
                self.sequenceDelete.place(x = self.sequenceFrame.winfo_width() - 6, y = 0, height=22, anchor="ne")
                self.sequenceEdit.place(x = self.sequenceDelete.winfo_x() - 1, y = 0, height=22, anchor="ne")
                self.sequenceNew.place(x = self.sequenceEdit.winfo_x() - 1, y = 0, height=22, anchor="ne")

                self.sequenceTextZone.place(x = self.sequenceFrame.winfo_width() / 4 * 3 + 12.5, y = 85 + (self.sequenceFrame.winfo_height() - 85) / 2, anchor = "center")

                current = self.sequenceList.curselection()
                if not current:
                    current = (0,)
                current = current[0]
                self.sequenceList.delete(0, self.sequenceList.size())
                for sequence_name, sequence in self.active_guide.sequences():
                    self.sequenceList.insert("end", sequence_name)
                self.sequenceList.activate(current)
                self.sequenceList.place(x = 0, y = 25, height = self.sequenceFrame.winfo_height() - 27, relwidth = 0.5)
                self.sequenceScroll.place(x = self.sequenceList.winfo_width(), y = 25, height = self.sequenceFrame.winfo_height() - 27)

                self.sequencePlay.place(x = self.sequenceFrame.winfo_width() / 2 + 10 + self.sequenceScroll.winfo_width(), y = 35, width = 50, height = 50)
                self.sequencePlayAndRecord.place(x = self.sequenceFrame.winfo_width() - 11, y = 35, width = 50 , height = 50, anchor = "ne")

                self.recordingFrame.place(x = 0, y = 0, height = 70, width = 325)

                self.infoZone.place(x = (self.winfo_width() + self.recordingFrame.winfo_width()) / 2, y = self.recordingFrame.winfo_height() / 2, anchor="center")

                self.sequenceFrame.place(x = 0, y = 70, width = self.winfo_width() / 2 - 2.5, height = self.winfo_height() - 70)

                self.scenarioFrame.place(x = self.winfo_width(), y = 70, width = self.winfo_width() / 2 - 2.5, height = self.winfo_height() - 70, anchor="ne")

            except:
                pass


    def __call__(self, *args: Any, **kwds: Any) -> Any:
        pass