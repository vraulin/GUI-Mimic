import tkinter as tk
from source.gui.var import freeVar

from source.objects.transforms import Transform


class TransformManager(tk.Toplevel):

    def link(self, transform : freeVar):
        from ..objects import transforms
        self.transformClasses = {"Transform" : transforms.Transform}
        self.transformVar = transform
        transform = transform.get()
        self.placers = []
        self.reshaping = False
        for name in dir(transforms):
            if isinstance(getattr(transforms, name), type) and issubclass(getattr(transforms, name), transforms.Transform) and not hasattr(getattr(transforms, name), "_secret"):
                self.transformClasses[name] = getattr(transforms, name)
        from tkinter import ttk
        self.transformPreviousType = True.__class__
        self.transformType = None.__class__
        self.currentType = tk.Variable(self)
        self.typeSelector = ttk.Combobox(self, values=list(self.transformClasses), state="readonly", textvariable=self.currentType)
        self.typeSelector.bind("<<ComboboxSelected>>", self.place_parameters)
        self.parameterDict = {}
        self.parameterFrames = []
        self.okButton = tk.Button(self, text="OK", command=self.create_transform)
        self.cancelButton = tk.Button(self, text="Cancel", command=self.destroy)
        
        self.title("Transform customization")
        self.iconbitmap("source/images/logo.ico")

        if transform:
            self.typeSelector.set(transform.__class__.__name__)
        else:
            self.typeSelector.set("Transform")

        self.refresh()

        self.bind("<Configure>", self.refresh)
        self.minsize(350, 300)

        self.master.attributes("-disabled", True)

        self.place_parameters()


    def create_transform(self, transform = None) -> Transform:
        try:
            from .selectors import _listing_table
            d = {name : value.get() for name, value in self.parameterDict.items() if name not in _listing_table}
            l = []
            for name, value in self.parameterDict.items():
                if name in _listing_table:
                    l.extend(value.get())
            transform = self.transformClasses[self.currentType.get()](*l, **d)
        except:
            from traceback import print_exc
            print_exc()
            from tkinter import messagebox
            if not self.parameterDict:
                messagebox.showerror(message="Cannot create abstract transform.")
            else:
                messagebox.showerror(message="Transform is missing some parameters.")
            return
        self.transformVar.set(transform)
        self.destroy()


    def destroy(self) -> None:
        self.master.attributes("-disabled", False)
        self.master.rebind()
        return super().destroy()


    def place_parameters(self, transform = None):
        t = self.currentType.get()
        if t:
            self.transformType = self.transformClasses[t]
        else:
            self.transformType = None.__class__
        

        if self.transformType != self.transformPreviousType:
            for frame in self.parameterFrames:
                frame.destroy()
            self.parameterFrames.clear()
            self.parameterDict.clear()
            self.placers.clear()
            transformType = self.transformClasses[self.currentType.get()]
            transform = self.transformVar.get()
            from .selectors import _name_translation_table, _selection_table
            for argument in transformType.__init__.__annotations__:
                if argument != "return":
                    default = None
                    if transform != None and hasattr(transform, argument):
                        default = getattr(transform, argument)
                    nameFrame = tk.Frame(self, borderwidth=1, relief="raised")
                    name = tk.Label(nameFrame, text=(argument.title() if argument not in _name_translation_table else _name_translation_table[argument]))
                    name.place(x = 0, y = 0)
                    self.parameterFrames.append(nameFrame)
                    frame = tk.Frame(self, borderwidth=1, relief="sunken")
                    self.parameterFrames.append(frame)
                    if argument in _selection_table:
                        param, placer = _selection_table[argument](frame, default)
                    else:
                        param, placer = _selection_table["default"](frame, argument)
                    self.parameterDict[argument] = param
                    placer()
                    self.placers.append(placer)
                    frame.update()
                    nameFrame.update()
            self.transformPreviousType = self.transformType

        self.refresh(20)

    
    def refresh(self, times : int = 3):

        if self.reshaping:
            return
        self.reshaping = True

        if not isinstance(times, int):
            times = 3
        
            
        for i in range(times):
            try:
                self.typeSelector.place(x = 0, y = 0, width = self.winfo_width())

                lasty = self.typeSelector.winfo_height()
                for i, frame in enumerate(self.parameterFrames):
                    frame.place(x = 0, y = lasty + (1 - i%2) * 20, relwidth = 1.0, height = max(child.winfo_y() + child.winfo_height() for child in frame.children.values()) + 2)
                    lasty = frame.winfo_y() + frame.winfo_height()

                for placer in self.placers:
                    placer()

                self.okButton.place(relx = 1 / 3, rely = 1.0, width = 75, anchor = "s")
                self.cancelButton.place(relx = 2 / 3, rely = 1.0, width = 75, anchor = "s")

                self.minsize(350, lasty + self.okButton.winfo_height())
            except:
                pass
        

        self.reshaping = False