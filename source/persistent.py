def load(path, *args, **kwargs):
    from pickle import load
    if isinstance(path, str):
        path = open(path, "rb")
    path.read(1)
    return load(path)[0][0]


def save(path, obj, *args, **kwargs):
    from pickle import dump
    if isinstance(path, str):
        path = open(path, "wb")
    path.write(b"\0")
    dump(((obj, ), {}), path)