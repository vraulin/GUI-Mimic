from abc import abstractmethod

from pynput.keyboard import KeyCode

from ..objects.user_sequence import UserSequence
from .events import Event, KeyboardEvent, KeyboardInput, KeyboardPress, KeyboardRelease, MouseEvent, MouseMove, MousePress, MouseRelease, MouseScroll, MouseStart, MouseStop
from typing import Any, Callable, Dict, List, Sequence, TypeVar, Union


class Transform:

    @abstractmethod
    def __init__(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def __call__(self, event : Event) -> Union[Event, Sequence[Event]]:
        raise NotImplemented


class InitializingTransform(Transform):

    @abstractmethod
    def initialize(self, sequence : UserSequence) -> None:
        raise NotImplemented


class CustomTransform(Transform):

    """
    A transformation class that enables to apply function to event sequences.
    A transformation requires a Callable that takes a single event as input and returns another event or a sequence of events (might be empty to delete the event).
    """

    def __init__(self, function : Callable[[Event, Any], Union[Event, Sequence[Event]]], *args : Any, **kwargs : Any) -> None:
        if not isinstance(function, Callable):
            raise TypeError("Expected Callable, got " + repr(function.__class__.__name__))
        try:
            from pickle import dumps
            dumps(function)
        except:
            raise TypeVar("Function is not part of an external module, cannot save it.")
        self.function = function
        self.args = args
        self.kwargs = kwargs

    
    def set_arguments(self, *args : Any, **kwargs : Any) -> None:
        self.args = args
        self.kwargs = kwargs
    

    def __call__(self, event : Event) -> Union[Event, Sequence[Event]]:
        try:
            return self.function(event, *self.args, **self.kwargs)
        except:
            import traceback
            raise ValueError("Given function did not work for " + str(event) + ":\n" + traceback.format_exc())




class TimeDilation(Transform):

    """
    A simple time dilation function.
    A time dilation of 1.0 does nothing. 2.0 double the duration. 0.5 halves it...
    """

    def __init__(self, factor : float = 1.0) -> None:
        if not isinstance(factor, float) or factor <= 0:
            raise TypeError("Expected nonzero positive float, got " + repr(factor.__class__.__name__))
        self.factor = factor

    
    def __call__(self, event : Event) -> Event:
        from copy import deepcopy
        E = deepcopy(event)
        E.time *= self.factor
        return E



class TimeNoise(Transform):

    """
    A simple time noise function. Adds Guassian relative noise in the time stamps.
    Argument is the variance of the Gaussian distribution.
    The law is 𝒩(1.0, variance) and the samples of this law will stretch the time stamps.
    For example, is the law is 𝒩(1.0, 0.3) (default), and the sample is 1.25, then, the given event
    will have its duration multiplied by 1.25.
    """

    def __init__(self, variance : float = 0.3) -> None:
        if not isinstance(variance, float) or variance <= 0:
            raise TypeError("Expected nonzero positive float, got " + repr(variance.__class__.__name__))
        self.variance = variance
        self.sigma = variance ** 0.5

    
    def __call__(self, event : Event) -> Event:
        from copy import deepcopy
        from random import gauss
        E = deepcopy(event)
        E.time *= max(gauss(1.0, self.sigma), 0) 
        return E


class FilterEvents(Transform):

    """
    A simple class-filter function for events.
    Just give it the classes of events you want to keep.
    The inverse keyword allows you in inverse the filtering.
    """

    def __init__(self, *classes : type, inverse : bool = False) -> None:
        for cls in classes:
            if not isinstance(cls, type):
                raise TypeError("Expected classes, got " + repr(cls.__class__.__name__))
        if not isinstance(inverse, bool):
            raise TypeError("Expected bool for inverse, got " + repr(inverse.__class__.__name__))
        
        self.inverse = inverse
        self.classes = classes

    
    def __call__(self, event : Event) -> List[Event]:
        
        if isinstance(event, self.classes) != self.inverse:
            return [event]
        return []





class RelativisticTime(Transform):

    """
    A simple transform to wind up times of event to local times, relative to the last event.
    """

    _secret = True

    def __init__(self) -> None:
        self.total = None
        
    
    def __call__(self, event : Event) -> Event:
        from copy import deepcopy
        e = deepcopy(event)
        if self.total == None:
            e.time, self.total = 0, e.time
        else:
            e.time, self.total = e.time - self.total, e.time
        return e



class SynthesizeMouseEvents(InitializingTransform):

    """
    Refines a sequence by marking the starts and stops of the mouse with the corresponding events.
    """

    def __init__(self, *, inactivity : int = 100000000, max_duration : int = 2000000000) -> None:
        if not isinstance(max_duration, int) or not isinstance(inactivity, int):
            raise TypeError("Expected int, int for inactivity and max_duration, got " + repr(inactivity.__class__.__name__) + " and " + repr(max_duration.__class__.__name__))
        
        self.inactivity = inactivity
        self.max_duration = max_duration

        self.markings : List[range] = []

        self.elapsed = 0
        self.counter = -1
        self.last : MouseEvent = None


    def initialize(self, sequence: UserSequence) -> None:
        self.markings = []

        self.elapsed = 0
        self.counter = -1
        self.last = None
        matching = False
        last = -1
        for i, ei  in enumerate(sequence):
            if not matching:
                if isinstance(ei, MouseMove):
                    last = i
                    matching = True
            else:
                if not isinstance(ei, MouseMove) or i == len(sequence) - 1:
                    matching = False
                    self.markings.append(range(last, i + isinstance(ei, MouseMove)))
    

    def __call__(self, event: Event) -> Union[Event, Sequence[Event]]:
        self.counter += 1
        if not isinstance(event, MouseMove) or not self.markings:
            return event
        r = self.markings[0]
        ret = []
        if self.counter in r:
            if self.counter == r.start:
                ret.append(MouseStart(max(event.time, 0), event.x, event.y))
            elif (event.time > self.inactivity or self.elapsed + event.time > self.max_duration) and self.last:
                self.elapsed, stop = 0, MouseStop(self.elapsed, self.last.x, self.last.y)
                ret.extend([stop, MouseStart(max(event.time, 0), event.x, event.y)])
            self.elapsed += max(event.time, 0)
            if self.counter == r.stop - 1:
                self.elapsed, event = 0, MouseStop(self.elapsed - (max(event.time, 0) * bool(ret)), event.x, event.y)
                self.markings.pop(0)
                ret.append(event)
        self.last = event
        return ret




class SynthesizeKeyboardEvents(InitializingTransform):

    """
    Refines a sequence by marking the simple contiguous keystrokes that form a str as KeyboardInput event.
    """

    def __init__(self, *, inactivity : int = 3000000000) -> None:
        if not isinstance(inactivity, int):
            raise TypeError("Expected int for inactivity, got " + repr(inactivity.__class__.__name__))
        
        self.inactivity = inactivity

        self.markings : List[range] = []

        self.counter = -1
        self.text = ""
        self.time = 0


    def initialize(self, sequence: UserSequence) -> None:
        self.markings = []
        self.counter = -1
        self.text = ""
        self.time = 0
        pressed = False
        lastkey = ""
        start = -1
        stop = -1
        for i, ei in enumerate(sequence):
            if pressed:
                if isinstance(ei, KeyboardRelease) and ei.key == lastkey:
                    stop = i + 1
                else:
                    stop = i - 1
                pressed = False
            else:
                if isinstance(ei, KeyboardPress) and isinstance(ei.key, KeyCode) and str(ei.key).islower() and len(str(ei.key)) <= 3 and ei.time < self.inactivity:
                    if stop == i:
                        stop += 1
                    else:
                        start = i
                    pressed = True
                    lastkey = ei.key
                else:
                    if stop == i:
                        self.markings.append(range(start, stop))
        if stop == len(sequence):
            self.markings.append(range(start, stop))


    def __call__(self, event: Event) -> Union[Event, Sequence[Event]]:
        self.counter += 1
        if not self.markings:
            return event
        if self.counter in self.markings[0] and isinstance(event, KeyboardEvent) and isinstance(event.key, KeyCode):
            if isinstance(event, KeyboardPress):
                self.text += str(event.key).replace("'", "")
            self.time += event.time
            if self.counter == self.markings[0].stop - 1:
                r = self.markings.pop(0)
                time, text = self.time, self.text
                self.text, self.time = "", 0
                return KeyboardInput(time // len(r), text, len(r) / time * 1000000000)
            return []
        return event




class CloseUnboundEvents(InitializingTransform):

    """
    Finds events that miss a corresponding event (ex: KeyboardPress without a KeyboardRelease) and adds the missing event at the beginning or at the end of the sequence.
    """

    def __init__(self) -> None:
        self.counter = -1
        self.opening_events = []
        self.len = -1
        self.closing_events = []
    
    
    def initialize(self, sequence: UserSequence) -> None:
        from pynput.mouse import Button
        self.counter = -1
        self.opening_events = []
        active = {}
        self.len = len(sequence)
        self.closing_events = []
        for i, event in enumerate(sequence):
            if isinstance(event, KeyboardPress):
                active[event.key] = i
            elif isinstance(event, MousePress):
                active[event.button] = i
            elif isinstance(event, KeyboardRelease):
                if event.key not in active:
                    self.opening_events.append(KeyboardPress(event.time, event.key))
                else:
                    active.pop(event.key)
            elif isinstance(event, MouseRelease):
                if event.button not in active:
                    self.opening_events.append(MousePress(event.time, event.x, event.y, event.button))
                else:
                    active.pop(event.button)
        for key_or_button, i in active.items():
            event = sequence[i]
            if isinstance(key_or_button, Button):
                self.closing_events.append(MouseRelease(event.time, event.x, event.y, key_or_button))
            else:
                self.closing_events.append(KeyboardRelease(event.time, key_or_button))
    

    def __call__(self, event: Event) -> Union[Event, Sequence[Event]]:
        l = []
        self.counter += 1
        if self.counter == 0:
            l.extend(self.opening_events)
        l.append(event)
        if self.counter >= self.len - 1:
            l.extend(self.closing_events)
        return l




cleaning_procedure = [CloseUnboundEvents, SynthesizeKeyboardEvents, SynthesizeMouseEvents]