from ..persistent import load

try:
    word_file = __file__[:-len(__name__) - 3] + r"source\text_data\Words.pyt"
    words = load(word_file)

    sentence_file = __file__[:-len(__name__) - 3] + r"source\text_data\Sentences.pyt"
    sentences = load(sentence_file)
except:
    from traceback import print_exc
    print_exc()

def uniform_random_word() -> str:
    """
    Returns (truly) a random English word.
    """
    from random import choice
    return choice(words)


def random_word() -> str:
    """
    Returns a random word with natural appearance frequencies.
    """
    from random import choice
    import re
    word_re = re.compile(r"[A-Za-z0-9-']+")
    return choice(word_re.findall(choice(sentences)))


def random_sentence() -> str:
    """
    Returns a random english sentence.
    """
    from random import choice
    return choice(sentences)


def random_input() -> str:
    """
    Returns a completely random text input of around hundreds of characters.
    """
    from random import normalvariate, randbytes, random
    n = max(10, round(normalvariate(150, 50)))
    s = ""
    while len(s) < n:
        try:
            s += str(randbytes(1 + (random() > 0.8) + (random() > 0.95) + (random() > 0.99)), encoding="utf-8")
        except:
            pass
    return s


class randrange:

    """
    Like randrange from the random module, except as a class which instances stores the start and end of the range. Pick a sample by calling the instance.
    """


    def __init__(self, start : int, stop : int) -> None:
        if not isinstance(start, int) or not isinstance(stop, int):
            raise TypeError("Expected int, int, got " + repr(start.__class__.__name__) + " and " + repr(stop.__class__.__name__))
        self.start = start
        self.stop = stop
    

    def __str__(self) -> str:
        return "randrange(" + str(self.start) + ", " + str(self.stop) + ")"
    
    __repr__ = __str__
    

    def __call__(self) -> int:
        from random import randrange
        return randrange(self.start, self.stop)