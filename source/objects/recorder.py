from typing import Any, Callable, List
from pynput import mouse, keyboard
from ..persistent import load, save
from time import time_ns


event_list = []

listening = False
hotkeys = {
    keyboard.Key.ctrl_l : False,
    keyboard.Key.ctrl_r : False
}
lock = False

origin = [time_ns()]

_on_start = []
_on_stop = []
_on_failed_start = []

def set_recording_hotkeys(*keys : keyboard.Key):
    """
    Changes the hotkeys used to start or stop recording. (Defaults to both Ctrl keys at the same time)
    """
    for key in keys:
        if not isinstance(key, keyboard.Key):
            raise TypeError("Expected keys, got " + repr(key.__class__.__name__))
    hotkeys.clear()
    for key in keys:
        hotkeys[key] = False


def get_recording_hotkeys() -> List[keyboard.Key]:
    return list(hotkeys)


def lock_recording():
    """
    Loacks the listener, making it impossible to start a recording.
    """
    global lock
    lock = True


def unlock_recording():
    """
    Releases the listener lock.
    """
    global lock
    lock = False


def trace_add(mode : str, func : Callable):
    """
    Adds a function to call when starting recording, stopping recording, both, or when a start attempt is failed because of the locking mechanism.
    mode should be "start", "stop", "both" or "locked".
    """
    if mode not in ("start", "stop", "both", "locked"):
        raise ValueError("Mode should be either 'start', 'stop', 'both' or 'locked', not " + repr(mode))
    if not callable(func):
        raise TypeError("func should be callable, got " + repr(func.__class__.__name__))
    if mode in ("start", "both"):
        _on_start.append(func)
    if mode in ("stop", "both"):
        _on_stop.append(func)
    if mode == "locked":
        _on_failed_start.append(func)


def trace_clear():
    """
    Clears all the trace functions.
    """
    _on_start.clear()
    _on_failed_start.clear()
    _on_stop.clear()


def start_listening() -> bool:
    """
    Starts the listener if no lock is active.
    Calls the on-start functions on success, and the on-locked functions on failure.
    """
    global listening, lock
    if lock:
        for func in _on_failed_start:
            try:
                func()
            except:
                print("Given on-locked-start function did not work:")
                from traceback import print_exc
                print_exc()
        return False
    listening = True
    for func in _on_start:
        try:
            func()
        except:
            print("Given on-start function did not work:")
            from traceback import print_exc
            print_exc()
    to_delete = [repr(i) for i in [keyboard.Key.ctrl_l, keyboard.Key.alt_l]]
    from .events import KeyboardPress, KeyboardRelease
    while event_list and isinstance(event_list[-1], (KeyboardPress, KeyboardRelease)) and repr(event_list[-1].key) in to_delete:
        to_delete.remove(repr(event_list[-1].key))
        event_list.pop()
    
    last_start[0] = len(event_list)
    origin[0] = time_ns()
    return True


def stop_listening():
    """
    Stops the listener.
    Also calls all the on-stop functions.
    """
    global listening
    listening = False
    for func in _on_stop:
        try:
            func()
        except:
            print("Given on-stop function did not work:")
            from traceback import print_exc
            print_exc()
    to_delete = [repr(i) for i in [keyboard.Key.ctrl_l, keyboard.Key.alt_l]]
    from .events import KeyboardPress
    while event_list and isinstance(event_list[-1], KeyboardPress) and repr(event_list[-1].key) in to_delete:
        event_list.pop()

last_start = [0]

Mc = mouse.Controller()


def on_move(x, y):
    from .events import MouseMove
    global listening
    if listening:
        event_list.append(MouseMove(time_ns() - origin[0], *Mc.position))

def on_click(x, y, button, pressed):
    from .events import MousePress, MouseRelease
    global listening
    if listening:
        if pressed:
            event_list.append(MousePress(time_ns() - origin[0], *Mc.position, button))
        else:
            event_list.append(MouseRelease(time_ns() - origin[0], *Mc.position, button))

def on_scroll(x, y, dx, dy):
    from .events import MouseScroll
    global listening
    if listening:
        event_list.append(MouseScroll(time_ns() - origin[0], *Mc.position, dx, dy))

def on_press(key):
    from .events import KeyboardPress
    global listening
    if key in hotkeys:
        hotkeys[key] = True
    if listening:
        event_list.append(KeyboardPress(time_ns() - origin[0], key))
    if all(hotkeys.values()):
        if listening:
            stop_listening()
        else:
            start_listening()

def on_release(key):
    from .events import KeyboardRelease
    global listening
    if key in hotkeys:
        hotkeys[key] = False
    if listening:
        if last_start[0] != len(event_list):
            event_list.append(KeyboardRelease(time_ns() - origin[0], key))



k_listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)

k_listener.start()


m_listener = mouse.Listener(
    on_move=on_move,
    on_click=on_click,
    on_scroll=on_scroll)

m_listener.start()





def show(ev = None):
    """
    With no arguments, shows the vent in the buffer sequence.
    With an iterable, shows all the elements of the iterable, one line per element.
    """
    if ev == None:
        ev = event_list
    for event in ev:
        print(repr(event))

from .user_sequence import UserSequence
from .events import Event

def extract(filter : Callable[[Event], bool] = lambda x : True) -> UserSequence:
    """
    Extracts all the events in the buffer sequence and returns a UserSequence.
    """
    # start_type and stop_type should be either "Mouse", "Keyboard" or "None"
    from .user_sequence import UserSequence
    r = event_list.copy()
    event_list.clear()
    seq = UserSequence(list(ei for ei in r if filter(ei))).apply(transforms.RelativisticTime())
    return seq



def clear():
    """
    Clears all the events currently stored in the buffer sequence.
    """
    event_list.clear()


from os import chdir
from .user_guide import UserGuide
from .user_scenario import UserScenario

env = {
    "load" : load,
    "save" : save,
    "show" : show,
    "UserGuide" : UserGuide,
    "UserSequence" : UserSequence,
    "UserScenario" : UserScenario,
    "extract" : extract,
    "clear" : clear,
    "trace_add" : trace_add,
    "trace_clear" : trace_clear,
    "lock" : lock_recording,
    "unlock" : unlock_recording,
    "start" : start_listening,
    "stop" : stop_listening,
    "set_hotkeys" : set_recording_hotkeys,
    "cd" : chdir,
    "Key" : keyboard.Key,
    "Button" : mouse.Button
}

from ..objects import events

for ni in dir(events):
    ei = getattr(events, ni)
    if isinstance(ei, type) and issubclass(ei, events.Event):
        env[ni] = ei

from ..objects import transforms

for name in dir(transforms):
    obj = getattr(transforms, name)
    if isinstance(obj, type) and issubclass(obj, transforms.Transform):
        env[name] = obj

from ..objects import hotkeys as hk

env["Hotkey"] = hk.Hotkey

def interactive():
    from os import chdir
    from os.path import dirname
    chdir(dirname(dirname(dirname(__file__))))
    from code import interact
    print("Interactive recorder. Press both ctrl keys to start or stop recording.")
    print("You can also explore the available functions with 'help(func)' and 'dir()'.")
    interact(local=env)