from abc import ABCMeta, abstractmethod

from pynput import keyboard
from .hotkeys import Hotkey
from typing import Any, Callable, List, Sequence, Union

from pynput.keyboard import Key, KeyCode
from pynput.mouse import Button

_Last_tick = -1
def _actual_pause(time : int) -> float:
    from time import time_ns
    global _Last_tick
    t = time_ns()
    
    if _Last_tick < 0:
        _Last_tick = t
    dt = t - _Last_tick
    return max(time - dt, 0) / 1000000000


def _correct(time : int) -> float:
    from time import time_ns
    global _Last_tick
    t = time_ns()
    dt = t - _Last_tick
    return max(time - dt, 0) / 1000000000


def _update() -> None:
    from time import time_ns
    global _Last_tick
    _Last_tick = time_ns()


class Event(metaclass = ABCMeta):

    """
    Abstract Base Class of events. Only has a time attribute representing how long to wait before executing the event in nanoseconds.
    """

    __slots__ = ("time")

    _generative = False

    @abstractmethod
    def __init__(self) -> None:
        self.time : int
        raise NotImplementedError
    
    def __str__(self) -> str:
        return self.__class__.__name__ + " event"
    
    def __repr__(self) -> str:
        return self.__class__.__name__ + "(" + ", ".join(ai + " = " + repr(getattr(self, ai)) for ai in self.__slots__) + ")"

    @abstractmethod
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        """
        Plays the event. The given arguments are the mouse and keyboard controllers.
        Note that, if it is supposed to, the event must sleep.
        If the class attribute "_generative" is True, this event must instead return the generated event sequence that it is supposed to generate.
        """
        raise NotImplementedError
    

    def copy(self) -> "Event":
        from copy import deepcopy
        return deepcopy(self)



class SleepEvent(Event):
    """
    Just an event that indicates the sequence needs a pause.
    """

    __slots__ = ("time",)

    def __init__(self, time : int) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        self.time = time
    

    def play(self, Mc, Kc, *, mouse_resolution: float = 0.02, smooth_mouse: bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        sleep(_correct(self.time))
        _update()


class KeyboardEvent(Event):

    """
    An Abstract Base Class for all keyboard events.
    """


class KeyboardPress(KeyboardEvent):

    """
    An event that indicates a key has been pressed on the keyboard. key attribute represents the corresponding key object.
    """

    __slots__ = ("time", "key")

    def __init__(self, time : int, key : Key) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if not isinstance(key, (KeyCode, Key, str)) or (isinstance(key, str) and len(key) != 1):
            raise TypeError("Expected Key, Keycode or str of length 1, got " + repr(key.__class__.__name__))
        self.time = time
        self.key = key

    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        Kc.press(self.key)
        sleep(_correct(self.time))
        _update()
    

class KeyboardRelease(KeyboardEvent):

    """
    An event that indicates a key has been released on the keyboard. key attribute represents the corresponding key object.
    """

    __slots__ = ("time", "key")

    def __init__(self, time : int, key : Key) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if not isinstance(key, (KeyCode, Key, str)) or (isinstance(key, str) and len(key) != 1):
            raise TypeError("Expected Key, Keycode or str of length 1, got " + repr(key.__class__.__name__))
        self.time = time
        self.key = key
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        Kc.release(self.key)
        sleep(_correct(self.time))
        _update()


class MouseEvent(Event):

    """
    An Abstract Base Class for all mouse events.
    """

    x : int
    y : int


class MouseMove(MouseEvent):

    """
    An event that indicates a mouse move. Its x and y attributes indicate the final position of the mouse.
    """

    __slots__ = ("time", "x", "y")

    def __init__(self, time : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]]) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        self.time = time
        self.x = x
        self.y = y
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        x, y = self.x, self.y
        if not isinstance(x, int):
            x = x()
        if not isinstance(y, int):
            y = y()
        try:
            for i in range(10):
                if Mc.position != (x, y):
                    Mc.position = (x, y)
        except:
            pass
        sleep(_correct(self.time))
        _update()


class MousePress(MouseEvent):

    """
    An event that indicates a mouse click. Its x and y attributes indicate the final position of the mouse.
    Its button attribute is the corresponding button object.
    """

    __slots__ = ("time", "x", "y", "button")

    def __init__(self, time : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]], button : Button) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        if not isinstance(button, Button):
            raise TypeError("Expected Button, got " + repr(button.__class__.__name__))
        self.time = time
        self.x = x
        self.y = y
        self.button = button
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        x, y = self.x, self.y
        if not isinstance(x, int):
            x = x()
        if not isinstance(y, int):
            y = y()
        try:
            for i in range(10):
                if Mc.position != (x, y):
                    Mc.position = (x, y)
        except:
            pass
        Mc.press(self.button)
        sleep(_correct(self.time))
        _update()


class MouseRelease(MouseEvent):

    """
    An event that indicates a mouse button release. Its x and y attributes indicate the final position of the mouse.
    Its button attribute is the corresponding button object.
    """

    __slots__ = ("time", "x", "y", "button")

    def __init__(self, time : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]], button : Button) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        if not isinstance(button, Button):
            raise TypeError("Expected Button, got " + repr(button.__class__.__name__))
        self.time = time
        self.x = x
        self.y = y
        self.button = button
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        x, y = self.x, self.y
        if not isinstance(x, int):
            x = x()
        if not isinstance(y, int):
            y = y()
        try:
            for i in range(10):
                if Mc.position != (x, y):
                    Mc.position = (x, y)
        except:
            pass
        Mc.release(self.button)
        sleep(_correct(self.time))
        _update()


class MouseScroll(MouseEvent):

    """
    An event that indicates a mouse scroll. Its x and y attributes indicate the final position of the mouse.
    Its dx and dy coordinates indicate the scroll direction and amount.
    """

    __slots__ = ("time", "x", "y", "dx", "dy")

    def __init__(self, time : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]], dx : Union[int, Callable[[], int]], dy : Union[int, Callable[[], int]]) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        if ((not isinstance(dx, int)) and not isinstance(dx, Callable)) or ((not isinstance(dy, int)) and not isinstance(dy, Callable)):
            raise TypeError("Expected integers or Callable for displacements, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        self.time = time
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> Union[None, List["Event"]]:
        from time import sleep
        sleep(_actual_pause(self.time))
        x, y, dx, dy = self.x, self.y, self.dx, self.dy
        if not isinstance(x, int):
            x = x()
        if not isinstance(y, int):
            y = y()
        if not isinstance(dx, int):
            dx = dx()
        if not isinstance(dy, int):
            dy = dy()
        try:
            for i in range(10):
                if Mc.position != (x, y):
                    Mc.position = (x, y)
        except:
            pass
        Mc.scroll(dx, dy)
        sleep(_correct(self.time))
        _update()


class MouseStart(MouseEvent):

    """
    An event that indicates the mouse started moving. Its x and y attributes indicate the starting of the mouse.
    """

    _generative = True

    __slots__ = ("time", "x", "y")

    def __init__(self, time : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]]) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        self.time = time
        self.x = x
        self.y = y
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> List["Event"]:
        if not smooth_mouse:
            x = self.x
            if not isinstance(x, int):
                x = x()
            y = self.y
            if not isinstance(y, int):
                y = y()
            return [MouseMove(self.time, x, y)]
        return [SleepEvent(self.time)]



class MouseStop(MouseEvent):

    """
    An event that indicates the mouse stopped moving. Its x and y attributes indicate the final position of the mouse.
    """

    _generative = True

    __slots__ = ("time", "x", "y")

    def __init__(self, time : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]]) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        self.time = time
        self.x = x
        self.y = y
    
    def play(self, Mc, Kc, *, mouse_resolution : float = 0.02, smooth_mouse : bool = False) -> List["Event"]:
        if self.time == 0:
            return [MouseMove(0, self.x, self.y)]
        x = self.x
        if not isinstance(x, int):
            x = x()
        y = self.y
        if not isinstance(y, int):
            y = y()
        start, stop = Mc.position, (x, y)
        MR = max(int(self.time / 1000000000 / mouse_resolution), 1)
        dx = 1 / MR
        dt = round(max(self.time / MR, 100000))
        from .Bezier import Bezier_curve, random_circle_point
        inter = random_circle_point(start, stop)
        C = Bezier_curve(start, inter, stop)
        l = []
        for i in range(MR):
            l.append(MouseMove(dt, *C(dx * (i + 1))))
        return l



class KeyboardInput(KeyboardEvent):

    """
    A generative event that create a an input from the user.
    The input can be a string or a sequence of Keys/Hotkeys objects
    The speed is the number of keys pressed by second.
    """

    __slots__ = ("time", "input", "speed")

    _generative = True

    def __init__(self, time : int, input : Union[str, Sequence[Key], Hotkey, Callable[[], Union[str, Sequence[Key], Hotkey]]], speed : float = 8.0) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positive integer for time, got " + repr(time.__class__.__name__))
        if not isinstance(input, (Sequence, Hotkey, Callable)):
            raise TypeError("Expected Sequence of str, Keys, or Hotkey for input, got " + repr(input.__class__.__name__))
        if not isinstance(speed, (int, float)) or speed <= 0:
            raise TypeError("Expected nonzero positive float for speed, got " + repr(speed.__class__.__name__))
        self.time = time
        self.input = input
        self.speed = speed
    

    def play(self, Mc, Kc, *, mouse_resolution: float, smooth_mouse: bool) -> List["Event"]:
        dt = round(1 / self.speed / 2 * 1000000000)
        events = []

        if isinstance(self.input, Callable):
            input = self.input()
        else:
            input = self.input

        if isinstance(input, Sequence):
            shift = False
            special = False
            specials = "~#{[|`\@]}¤"
            k1 = keyboard.KeyCode.from_dead("~")
            k2 = keyboard.KeyCode.from_dead("`")
            k3 = keyboard.KeyCode.from_dead("¨")
            k4 = keyboard.KeyCode.from_dead("^")
            complexes = {a:b for a, b in zip(
                "ãõñàèùìòäëÿüïöâêûîô^", (
                    (k1, "a"),
                    (k1, "o"),
                    (k1, "n"),
                    (k2, "a"),
                    (k2, "e"),
                    (k2, "u"),
                    (k2, "i"),
                    (k2, "i"),
                    (k3, "a"),
                    (k3, "e"),
                    (k3, "y"),
                    (k3, "u"),
                    (k3, "i"),
                    (k3, "o"),
                    (k4, "a"),
                    (k4, "e"),
                    (k4, "u"),
                    (k4, "i"),
                    (k4, "o"),
                    "^ "
                    ))}
            input = list(input)
            copy = []
            for s in input:
                if isinstance(s, str) and s.lower() in complexes:
                    copy.extend(complexes[s])
                else:
                    copy.append(s)
            for s in copy:
                if isinstance(s, str) and (s.isupper() or s in "¨£%µ§/.?1234567890°+") and not shift:
                    events.append(KeyboardPress(dt, keyboard.Key.shift))
                    shift = True
                elif shift:
                    events.append(KeyboardRelease(dt, keyboard.Key.shift))
                    shift = False
                if isinstance(s, str) and s in specials and not special:
                    events.append(KeyboardPress(dt, keyboard.Key.alt_gr))
                    special = True
                elif special:
                    events.append(KeyboardRelease(dt, keyboard.Key.alt_gr))
                    special = False
                events.append(KeyboardPress(dt, s))
                events.append(KeyboardRelease(dt, s))
            if shift:
                events.append(KeyboardRelease(dt, keyboard.Key.shift))
            if special:
                events.append(KeyboardRelease(dt, keyboard.Key.alt_gr))

        
        elif isinstance(input, Hotkey):
            l = list(input)
            n = len(l)
            for s in l[:n // 2]:
                events.append(KeyboardPress(2 * dt, s))
            
            events.append(KeyboardRelease(4 * dt, l[n // 2]))

            for s in l[n // 2 + 1:]:
                events.append(KeyboardRelease(dt, s))


        events[0].time = self.time

        return events


class MouseClick(MouseEvent):

    """
    A generative event that represents moving the mouse to a certain place and clicking there.
    times indicates the number of clicks (default 1, 2 for double-click, 0 can actually be used to simply move the mouse...)
    """

    
    __slots__ = ("time", "dt", "x", "y", "button")

    _generative = True

    def __init__(self, time : int, dt : int, x : Union[int, Callable[[], int]], y : Union[int, Callable[[], int]], button : Button = Button.left, *, times : int = 1) -> None:
        if not isinstance(time, int) or time < 0:
            raise TypeError("Expected positàive integer for time, got " + repr(time.__class__.__name__))
        if not isinstance(dt, int) or dt < 0:
            raise TypeError("Expected positive integer for dt, got " + repr(dt.__class__.__name__))
        if not isinstance(button, Button):
            raise TypeError("Expected Button, got " + repr(button.__class__.__name__))
        if (not isinstance(x, int) and not isinstance(x, Callable)) or (not isinstance(y, int) and not isinstance(y, Callable)):
            raise TypeError("Expected integers or Callable for coordinates, got " + repr(x.__class__.__name__) + " and " + repr(y.__class__.__name__))
        if not isinstance(times, int) or times < 0:
            raise TypeError("Expected positive integer for times")
        self.time = time
        self.dt = dt
        self.button = button
        self.x = x
        self.y = y
        self.times = times
    
    def play(self, Mc, Kc, *, mouse_resolution: float, smooth_mouse: bool) -> List["Event"]:
        events = []

        events.append(MouseStop(self.time, self.x, self.y))

        for i in range(self.times):
            events.append(MousePress(self.dt, self.x, self.y, self.button))
            events.append(MouseRelease(self.dt // 2, self.x, self.y, self.button))
        
        return events
