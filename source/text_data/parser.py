import re

sentence = re.compile(r"[A-Z0-9][A-Za-z0-9-']*(?:[,;:]?\s[A-Za-z0-9-']+)*[.?!]")
word = re.compile(r"[A-Za-z0-9-']+")

from os import listdir
from os.path import dirname

sentences, words = [], []

for path in listdir(dirname(__file__)):
    path = dirname(__file__)+ "/" + path
    if path.endswith(".txt"):
        with open(path, "r", encoding="utf-8") as f:
            data = f.read()
            sentences.extend(sentence.findall(data))
            words.extend(word.findall(data))

from random import shuffle
shuffle(sentences)
shuffle(words)

from ..persistent import save

save(dirname(__file__) + "/Sentences.pyt", sentences)
save(dirname(__file__) + "/Words.pyt", words)