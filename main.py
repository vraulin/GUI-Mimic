from sys import argv

def printUsage():
    print("Usage:")
    print("py main.py                  --> Starts GUI-Mimic in graphic mode.")
    print("py main.py -I/-interactive  --> Starts GUI-Mimic with a Python interactive interpreter.")
    exit()

def GUIMode():
    from os import chdir
    from os.path import dirname
    chdir(dirname(__file__))
    from source.gui.root import GUIInterface
    app = GUIInterface()
    app.mainloop()

inter = False

if "-interactive" in argv or "-I" in argv:
    if "-I" in argv:
        argv.remove("-I")
        inter = True
    if "-interactive" in argv:
        argv.remove("-interactive")
        inter = True

if len(argv) > 1:
    printUsage()


if inter:
    from source.objects.recorder import interactive
    interactive()
else:
    GUIMode()